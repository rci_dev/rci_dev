/** 
* @author: Adolphe SONKO
* @date : Creation 01/08/2017
* @date : Modification 01/08/2017
* @description : This Class allows to handle trigger events in invoice entity.
*/
public class SM002_InvoiceTriggerHandler {
        
    private static String  CLASS_NAME = SM002_InvoiceTriggerHandler.class.getName();
    
    public static void onAfterInsert(List<Zuora__ZInvoice__c> invoices){
  
        String methodName = 'onAfterInsert';  
		    ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [invoices] : ' + invoices);
        
        List<Id> invoiceIds = new List<Id>();
        List<String> zuoraIds = new List<String>();
           
        for(Zuora__ZInvoice__c i : invoices){
              if( !ObjectUtil.isEmpty(i.Zuora__Zuora_Id__c) ) {
                    invoiceIds.add(i.Id);
                    zuoraIds.add(i.Zuora__Zuora_Id__c);  
              }
        }    
        
        if(invoiceIds != null && invoiceIds.size()>0)
        {
          	callgetPostInvoiceSubscriptionNumber(invoiceIds, zuoraIds);
        }          
    } // End method onAfterInsert
        
    
    /**
     * @author: Adolphe SONKO
	   * @date : Création 02/02/2017
	   * @date : Modification 02/02/2017
     * @description : 
     * @param : List<Id> listInvoiceIds.
     * @return : -Non-
     */
     
    @future(callout=true)
    public static void callgetPostInvoiceSubscriptionNumber(List<Id> listInvoiceIds, List<String> zuoraIds){
            
      String methodName = 'callgetPostInvoiceSubscriptionNumber'; 
      
      ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [listInvoiceIds] : ' + listInvoiceIds);
      
      Map<String,List<String>> mapListSubscriptionNumberByInvoiceIds = new Map<String,List<String>>();
      Map<String,String> mapSubscriptionNumberByInvoiceIdToLink = new Map<String,String>();
      Set<String> SubscriptionNumbers = new Set<String>();
      List<Zuora__Subscription__c> listSubscriptions= new List<Zuora__Subscription__c>();
      List<Zuora__ZInvoice__c> listInvoicesToReturn =  new List<Zuora__ZInvoice__c>();  
      Set<Id> accountIds = new Set<Id>();
      String country = ''; 

      //Savepoint sp = Database.setSavepoint();  
      try{	
          //Get the URL of the environment 
          ZuoraParameters__c urlEnv = EM002_Invoice.getRESTURL();        
                          
          List<Zuora__ZInvoice__c> listInvoices = EM002_Invoice.getListByInvoiceIds(listInvoiceIds);
                      
          if(listInvoices != null && listInvoices.size() > 0){
            for(Zuora__ZInvoice__c inv: listInvoices){
              accountIds.add(inv.Zuora__Account__c);
              break;
            }
            
            List<Account> listAccounts = EM001_Account.getListByIds(accountIds);
            for(Account acc: listAccounts){
              country = acc.AccountCountry__c;
              break;
            }
          }
                     
          //Get the billing entity  
          zqu__BillingEntity__c billingEntity = EM001_Account.getListBillingEntitiesByCountry(country);
                            
          if(billingEntity != null){
            // Connexion to Zuora instance
            //zApiInstance = null;
            Zuora.zApi zApiInstance = new Zuora.zApi();
            
            if(!Test.isRunningTest()){
              if ((billingEntity.zqu__EntityName__c == 'BR') ||(billingEntity.zqu__EntityName__c == 'DE')||(billingEntity.zqu__EntityName__c == 'UK')||(billingEntity.zqu__EntityName__c == 'PL')){
                zApiInstance.zlogin(billingEntity.zqu__EntityID__c);
              } 
              else{
                zApiInstance.zlogin();
              }
            }                       
            
            /******** Si la taille de la liste à insérer > 200 (batch bill run), spliter la liste plusieurs petites listes de taille 200 **************/ 
            /********************** Et Faire une boucle à partir d'ici. Exemple: Si 800 Invoices à insérer, diviser en 4 petites listes de 200 et boucler 4 fois****************************************/
            /***********************************************************************************************/
            //Request to Etouches Clone

            List<List<String>> LstAllIds = new List<List<String>>();
            //BUIDLING A LIST OF LIST OF 200 INVOICES
            for(Integer i = 0 ; i < (zuoraIds.size() / 200)+1 ; i++){
              List<String> lstTemp = new List<String>();
              
              for(Integer j=(i*200);(j<(i*200)+200) && j<zuoraIds.size() ; j++){
                lstTemp.add(zuoraIds.get(j));
              }
              
              LstAllIds.add(lstTemp);
            }

            for(List<String> lstZuoraIdSplitted: LstAllIds){
            //loop through list of lists
              HttpRequest request = EM002_Invoice.getPostInvoiceSubscriptionNumber(urlEnv, lstZuoraIdSplitted, billingEntity.zqu__EntityName__c);
                          
              system.debug('ADBS request     : ' + request);
              system.debug('ADBS request body: ' + request.getBody());
              //Send request 
                              
              HttpResponse responseInvoice = EM002_Invoice.createSubscriptionNumber(request, null, zApiInstance);
              system.debug('ADBS response     : ' + responseInvoice);
              system.debug('ADBS response body: ' + responseInvoice.getBody());
              system.debug('ADBS response getStatus   : ' + responseInvoice.getStatus());
              system.debug('ADBS response getStatusCode  : ' + responseInvoice.getStatusCode());
                          
              if(responseInvoice.getStatusCode() == 200){
                String JsonInvoiceSubscriptionInfos = responseInvoice.getBody();

                InvoiceSubscriptionInfos ISubInfos = (InvoiceSubscriptionInfos)JSON.deserialize(JsonInvoiceSubscriptionInfos, InvoiceSubscriptionInfos.class);
                                
                system.debug('List Invoices : '+ISubInfos);
                              
                if(ISubInfos != null && ISubInfos.records != null && ISubInfos.records.size() > 0){
                  for(SubscriptionInfos sub: ISubInfos.records){
                    if(mapListSubscriptionNumberByInvoiceIds != null && mapListSubscriptionNumberByInvoiceIds.get(sub.InvoiceId) != null){
                        mapListSubscriptionNumberByInvoiceIds.get(sub.InvoiceId).add(sub.SubscriptionNumber);
                    }
                    else{
                        mapListSubscriptionNumberByInvoiceIds.put(sub.InvoiceId, new list<String>{sub.SubscriptionNumber});
                    }
                  }
                                  
                  for(String invId: mapListSubscriptionNumberByInvoiceIds.keySet()){
                    if(mapListSubscriptionNumberByInvoiceIds != null && mapListSubscriptionNumberByInvoiceIds.get(invId) != null && mapListSubscriptionNumberByInvoiceIds.get(invId).size() == 1){
                        SubscriptionNumbers.add(mapListSubscriptionNumberByInvoiceIds.get(invId).get(0));
                                           mapSubscriptionNumberByInvoiceIdToLink.put(invId, mapListSubscriptionNumberByInvoiceIds.get(invId).get(0));
                    }
                  }                                  
                }//end if ISubinfos 
              } //end responseInvoice  
            }//end for            
            /*************************** Jusque là***************************************/
            /***************************************************************************/
      }//catch
                        
                       
      if(SubscriptionNumbers != null && SubscriptionNumbers.size() > 0){
        listSubscriptions = EM003_Subscription.getListBySubscriptionNumber(SubscriptionNumbers) ;
        if(listSubscriptions != null && listSubscriptions.size() > 0){
          for(Zuora__ZInvoice__c inv: listInvoices){
            for(Zuora__Subscription__c zsub: listSubscriptions){
              if(mapSubscriptionNumberByInvoiceIdToLink.get(inv.Zuora__Zuora_Id__c) != null && mapSubscriptionNumberByInvoiceIdToLink.get(inv.Zuora__Zuora_Id__c) == zsub.Name){
                    inv.SubscriptionName__c = zsub.Id;
                    listInvoicesToReturn.add(inv);
                }
              }
            }
        }
                          
        if(listInvoicesToReturn != null && listInvoicesToReturn.size() > 0){
          update listInvoicesToReturn;
          system.debug('here the result to insert :'+listInvoicesToReturn);
        } 
      }   
                       
                    
    }catch(DmlException e){
      String erreurMessage = e.getMessage();
      System.debug('Erreur :'+ erreurMessage ) ;
      //Database.rollback(sp);
    }catch(Exception e){
      String erreurMessage = e.getMessage();
      System.debug('Erreur :'+ erreurMessage ) ;
    //Database.rollback(sp);
    }    
  }
   
    
   public class SubscriptionInfos{
        public String SubscriptionNumber {get;set;}
        public String Id {get;set;}
        public String InvoiceId {get;set;}
   }
    
   public class InvoiceSubscriptionInfos{
        public List<SubscriptionInfos> records {get;set;}
        public String size {get;set;}
        public String done {get;set;}
   }
    
}