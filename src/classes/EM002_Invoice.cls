/** 
* @author: Adolphe SONKO
* @date : Creation 01/08/2017
* @date : Modification 01/08/2017
* @description : this Class allow to manage Invoice business entity data preparation .
*/

public class EM002_Invoice {
    
        
    private static String CLASS_NAME = 'EM002_Invoice';
    private static String HTTP_METHOD_POST = 'POST';
    private static String POST_CREATE_SUBSCRIPTION_NUMBER = '/v1/action/query';
	private static String CONTENT_TYPE = 'application/json';
    public static String NAME_URL_US = 'SBX US';
    public static String NAME_URL_EU = 'SBX EU';
    public static String NAME_URL_PROD = 'PROD EU';
    public static String SFDC_URL_US = 'https://cs88.salesforce.com';
    public static String SFDC_URL_PROD = 'https://eu11.salesforce.com';
  
    
    private static ITF001_getSubscriptionNumberFromInvoice postCreateInvoiceSubsriptionNumber_STUB {get {return ZuoraInvoice1_STUB();}}
  
    private static ITF001_getSubscriptionNumberFromInvoice ZuoraInvoice1_STUB(){
        String methodName = 'ZuoraInvoice1_STUB';
        ITF001_getSubscriptionNumberFromInvoice STUB;
        
        if (Test.isRunningTest()){
          // BGU NEED Class test for salesforce testing
          //  STUB = new MCK005_Campaign();
          //STUB = new MCK001_WebsiteContactWS();
          system.debug('BGU Test Need to developp class');
        } else {
           STUB = new DM002_Invoice();
           system.debug('BGU arrivé jusquici : method officiel');
        }
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName , '[RESULTS] : ' + STUB);
        return STUB;     
    }
   
    /**
    * @auteur :Adolphe SONKO
    * @date : Création 01/08/2017
    * @date : Modification 01/08/2017
    * @description : This method create a http request to create the mailing list
    * @param : ZuoraParameters__c baseUrl This parameter is the parameterss.
    * @return : HttpRequest The request
    */
    public static HttpRequest getPostInvoiceSubscriptionNumber(ZuoraParameters__c baseUrl, List<String> listZuoraIds, String entityName){
      
      String methodName = 'getPostInvoiceSubscriptionNumber';
      
      ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[param_setting] : ' + baseUrl);
      
      HttpRequest request = getCreateInvoiceInfos(baseUrl, HTTP_METHOD_POST, POST_CREATE_SUBSCRIPTION_NUMBER, listZuoraIds, entityName);
        
      ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[request] : ' + request);
      return request;
    }
    
    
    private static HttpRequest getCreateInvoiceInfos(ZuoraParameters__c baseUrl, String httpMethod, String ExpectedWebService, List<String> listZuoraIds, String entityName){
      
       String methodName = 'getCreateInvoiceInfos';
       ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[param_setting] : ' + baseUrl);
       ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[httpMethod] : ' + httpMethod);
      
       HttpRequest request = new HttpRequest();
       request.setEndpoint(baseUrl.URL__c + ExpectedWebService);
       request.setMethod(httpMethod);
		   request.setHeader('Content-Type', CONTENT_TYPE);
		   request.setHeader('Accept', CONTENT_TYPE);
       request.setHeader('entityname', entityName);

        // Create body      
        String query =  'select SubscriptionNumber, InvoiceId From InvoiceItem where ' ;
        String res = ObjectUtil.buildOrQueryFilter('InvoiceId', listZuoraIds, query, null, null);
         
     	  String requestBody = '{ "queryString":"'+res+'"}';
         
        request.setBody(requestBody);
        //request.setTimeout(120000);
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[request] : ' + request);
      return request;
    }
	
   
   public static HttpResponse createSubscriptionNumber(HttpRequest request,  String responseType, Zuora.ZApi zApiInstance){
        
        String methodName = 'createSubscriptionNumber';
        
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[request] :' + request);
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[request body] :' + request.getBody());
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[response Type] :' + responseType);
        
        HttpResponse response = null;

        if(!Test.isRunningTest()){
          response = postCreateInvoiceSubsriptionNumber_STUB.getPostSubscriptionNumber(request, responseType, zApiInstance); 
        }
        else{//used to mock test result
          
          List<Zuora__ZInvoice__c> inv = [SELECT ID from Zuora__ZInvoice__c WHERE Zuora__Zuora_Id__c = 'A-S99999999']; 
          system.debug('### INVOICE' + inv);
          response = new HttpResponse();
          response.setBody('{ "records" : [{ "SubscriptionNumber" : "A-S99999999", "Id" : "123", "InvoiceId" : "'+ inv[0].ID+'" }], "size" : "1", "done" : "true" }');

          response.setStatusCode(200);
        }//end mock response
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + response);              
        return response ;    
    }
    
    
    public static List<Zuora__ZInvoice__c> getListByInvoiceIds(List<Id> InvoiceIds){
        
        String methodName = 'getListByInvoiceIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[InvoiceIds] :' + InvoiceIds);
        
        List<Zuora__ZInvoice__c> listInvoices = DM002_Invoice.getListByInvoiceIds(InvoiceIds);
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listInvoices);
        return listInvoices; 
    }
	
    public static ZuoraParameters__c getRESTURL(){
        
        String methodName = 'getRESTURL';    
        ZuoraParameters__c urlEnv = new ZuoraParameters__c();
        ZuoraParameters__c urlEnv2 = new ZuoraParameters__c();
        ZuoraParameters__c urlEnv3 = new ZuoraParameters__c();
        
        //Get the URL of the environment
        urlEnv = ObjectUtil.getZuoraParametersByName(NAME_URL_US);
        urlEnv2 = ObjectUtil.getZuoraParametersByName(NAME_URL_PROD);
        String orgId = UserInfo.getOrganizationId();

        if(orgId == urlEnv.SFDC_OrgId__c){
          urlEnv3 = urlEnv;
        }else if(orgId == urlEnv2.SFDC_OrgId__c){
			    urlEnv3 = urlEnv2;
        }else{
          urlEnv3 = ObjectUtil.getZuoraParametersByName(NAME_URL_EU);
        }
                
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[URL] :' + urlEnv);
        return urlEnv3; 
    }
}