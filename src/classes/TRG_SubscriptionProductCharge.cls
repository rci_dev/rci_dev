/**
* @author BGU
* @date Creation 24/07/2017
* @date Modification 22/09/2017
* @description : TRG_SubscriptionProductCharge Class, manage the ID for several product
*/


public with sharing class TRG_SubscriptionProductCharge {
    
    public static void updateBSPFields  (List<Zuora__SubscriptionProductCharge__c> lsSubPrdChge) {
               
        
        System.debug('### Inside method');
        Zuora__SubscriptionProductCharge__c productCharge;     
        // Get current SubscriptionProductCharge
        for(Zuora__SubscriptionProductCharge__c prdt : lsSubPrdChge){
            productCharge = prdt;
        }
        
        System.debug('### productCharge ' + productCharge);
        
        // Get related Subscription
        Zuora__Subscription__c subscription = [SELECT id, name, Policy_Number__c, VoucherNumber_PL__c FROM Zuora__Subscription__c WHERE id =: productCharge.Zuora__Subscription__c LIMIT 1];
        System.debug('### subscription ' + subscription);
        System.debug('### subscription.Policy_Number__c ' + subscription.Policy_Number__c);
        System.debug('### subscription.VoucherNumber_PL__c ' + subscription.VoucherNumber_PL__c);
        
        
        
        IdVoucherNumber__c settingVoucher = IdVoucherNumber__c.getAll().get('Voucher');
        
        
        IdPolicyNumber__c settingPolicy =   IdPolicyNumber__c.getAll().get('Policy');
        
        String SKUProductCharge; 
        
        if(subscription.Policy_Number__c == null)
        {
            
            
            
            if(productCharge.Zuora__ProductSKU_Product__c != null)
            {
                
                SKUProductCharge = String.valueOf(productCharge.Zuora__ProductSKU_Product__c);
                System.debug('### BGU SKUProductCharge ' + SKUProductCharge);
                try {  
                    
                    if (checkCondition (SKUProductCharge, 'ConciergeClub')) {
                        
                        System.debug('### ConciergeClub');
                        //Get value
                        Integer counter = Integer.valueof(settingPolicy.Concierge__c);
                        counter = counter +1; 
                        
                        //Set name.
                        subscription.Policy_Number__c  = nextId(counter,'AFF/RCI/0002-CON');
                        
                        
                        // Set for next ID
                        settingPolicy.Concierge__c ++;
                        
                        // Update Setting
                        update settingPolicy;     
                        update subscription;  



                    }else{
                        System.debug('NOT ConciergeClub');
                    }
                    
                    if (checkCondition (SKUProductCharge, 'TyreInsurance')) {
                        
                        System.debug('### TyreInsurance');
                        //Get value
                        integer counter = Integer.valueof(settingPolicy.Tyre__c);
                        counter = counter +1; 
                        
                        //Set name.
                        subscription.Policy_Number__c  = nextId(counter,'AFF/RCI/0001-OPN');
                        
                        
                        // Set for next ID
                        settingPolicy.Tyre__c ++;
                        
                        // Update Setting
                        update settingPolicy;     
                        update subscription;
                        
                        
                        
                    }else{
                        System.debug('NOT TyreInsurance');
                    }
                    
                    
                    if (checkCondition (SKUProductCharge, 'CyberRisk')) {
                        
                        
                        System.debug('### CyberRisk');
                        //Get value
                        integer counter = Integer.valueof(settingPolicy.Cyber__c);
                        counter = counter +1; 
                        
                        //Set name. Old version of BGU
                    //    subscription.Policy_Number__c  = nextId(counter,'IPA-');  
                        
                    // Fix ASO 14/09/2017 New version
                    subscription.Policy_Number__c  = nextId(counter,'4531');
                        
                        // Set for next ID
                        settingPolicy.Cyber__c ++;
                        
                        // Update Setting
                        update settingPolicy;
                        update subscription; 
                        
                        
                    }else{
                        System.debug('NOT CyberRisk');
                    }
                    
                    
                    
                    
                }catch (exception e) {
                    
                    System.debug('Exception survenue ' + e.getMessage());
                }
                
                
                
                
            }
                
            
        }
        
        
        if(subscription.VoucherNumber_PL__c == null)
        {
            
            if (settingVoucher.Voucher_Number__c < settingVoucher.Voucher_Number_Max__c)
            {
                
                SKUProductCharge = String.valueOf(productCharge.Zuora__ProductSKU_Product__c);
                
                if (checkCondition (SKUProductCharge, 'CyberRisk') 
                    || checkCondition (SKUProductCharge, 'TyreInsurance') 
                    || checkCondition (SKUProductCharge, 'ConciergeClub') 
                    || checkCondition (SKUProductCharge, 'TravelInsurance'))
                    {
                        
                        settingVoucher.Voucher_Number__c ++;
                        subscription.VoucherNumber_PL__c =  nextId(Integer.valueof(settingVoucher.Voucher_Number__c), 'DT/H/0000/');
                
                        update settingVoucher;
                        update subscription;
                        
                        
                    }
                
            
                
            }
            
        }
        
        
      
    
    }


 private static String nextId(integer currentCount, String productName){
        
        String name;
        String sCounter = String.valueOf(currentCount);
        boolean test = true;
     	String tmpProduct;
      
    //OLD Version
    /*    while (test){        
             
            if (sCounter.length() < 8) {
                
                sCounter = '0' + sCounter ;
                
            }else
            {
                test = false;
            }
            
            
        }  */             
		
		// Fix ASO 22/09/2017 New version
		if(productName == 'AFF/RCI/0002-CON'){
				tmpProduct = 'ConciergeClub';
		}else if(productName == '4531'){
				tmpProduct = 'CyberRisk';
		}else if(productName == 'AFF/RCI/0001-OPN'){
				tmpProduct = 'TyreInsurance';
		}
		
		if(tmpProduct == 'TyreInsurance' || tmpProduct == 'ConciergeClub'){  
			while (test){        
				 
				if (sCounter.length() < 7) {
					
					sCounter = '0' + sCounter ;
					
				}else {
					test = false;
				}
			}
        }else if(tmpProduct == 'CyberRisk'){
			while (test){        
						 
				if (sCounter.length() < 6) {
					
					sCounter = '0' + sCounter ;
					
				}else {
					test = false;
				}		
						
			}
		} 
        
        name = productName + sCounter;
        return name;
        
    }

   
    
    
    // Compare the field Zuora__ProductSKU_Product__c with default values 
    private static boolean checkCondition (String SKUProductCharge, String identifier) {
        
        System.debug('### Start checkCondition. Identifier: ' + identifier);
        String sku = null;
        if (identifier.equals('ConciergeClub')) 
        {
         sku = Label.SKUConciergeClub;    
        }
              
        
        
        if (identifier.equals('TyreInsurance'))
        {
            sku = Label.SKUTyreInsurance; 
        }
            
        
        if (identifier.equals('CyberRisk'))
        {
            sku = Label.SKUCyberRisk;
        }
               
        
         if (identifier.equals('TravelInsurance'))
         {
           sku = Label.SKUTravelInsurance;  
         }
            
        
        System.debug('### sku: ' + sku);
        System.debug('### SKUProductCharge: ' + SKUProductCharge);
        If (sku != null)
        {
            If( SKUProductCharge.equals (sku)) 
            {
              return true;  
            }
            
        }
            
        
        System.debug('### Returning false ');
        return false;
        
    }
    
    
}