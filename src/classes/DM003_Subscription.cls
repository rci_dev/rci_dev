/** 
* @author: Adolphe SONKO
* @date : Création 02/02/2017
* @date : Modification 02/02/2017
* @description : this Class allow to manage all database operations (such as select requests and dml) with Salesforce.com .
*/
public class DM003_Subscription {
    
    private static String CLASS_NAME = 'DM003_Subscription';
    private static final String ALL_FIELDS = ObjectUtil.constructFieldListForQueryFrom('Zuora__Subscription__c');
    
    
    /**
     * @author : Adolphe SONKO
     * @date : Création 02/02/2017
	 * @date : Modification 02/02/2017
     * @description : This method performs an SQL query on the subscription to repatriate its data from an ID list.
     * @param : Set<Id> SubscriptionIds This parameter is a list of unique identifiers of subscription.
     * @return : List<Zuora__Subscription__c> The returned item is a list of Subscriptions.
     */  
    public static List<Zuora__Subscription__c> getListByIds(Set<Id> SubscriptionIds){
        
        String methodName = 'getListByIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[SubscriptionIds] :' + SubscriptionIds);
        
        String soqlQuery = 'SELECT ' + ALL_FIELDS + ' FROM Zuora__Subscription__c WHERE Id IN: SubscriptionIds';    
        List<Zuora__Subscription__c> listSubscriptions = new List<Zuora__Subscription__c>((List<Zuora__Subscription__c>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listSubscriptions);
        return listSubscriptions; 
    }
    
    
    /**
     * @author : Adolphe SONKO
     * @date : Création 02/02/2017
	 * @date : Modification 02/02/2017
     * @description : This method performs an SQL query on the subscription to repatriate its data from an String list.
     * @param : Set<String> subscriptionNumbers This parameter is a list of unique identifiers of subscription.
     * @return : List<Zuora__Subscription__c> The returned item is a list of Subscriptions.
     */  
    public static List<Zuora__Subscription__c> getListBySubscriptionNumber(Set<String> subscriptionNumbers){
        
        String methodName = 'getListBySubscriptionNumber';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[subscriptionNumbers] :' + subscriptionNumbers);
        
        String soqlQuery = 'SELECT ' + ALL_FIELDS + ' FROM Zuora__Subscription__c WHERE Name IN: subscriptionNumbers';    
        List<Zuora__Subscription__c> listSubscriptions = new List<Zuora__Subscription__c>((List<Zuora__Subscription__c>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listSubscriptions);
        return listSubscriptions; 
    }

}