/** 
* @author: Adolphe SONKO
* @date : Creation 01/08/2017
* @date : Modification 01/08/2017
* @description : his Class allows to Call the Webservices for zuora invoice subscriptin number 
*/

public class DM002_Invoice implements ITF001_getSubscriptionNumberFromInvoice {
	
    private static String CLASS_NAME = 'DM002_Invoice';
    private static final String ALL_FIELDS = ObjectUtil.constructFieldListForQueryFrom('Zuora__ZInvoice__c');

		/** 
		* @author: Adolphe SONKO
		* @date : Creation 01/08/2017
		* @date : Modification 01/08/2017
		* @description : This Methods call webservices method
     	* @param : HttpRequest request This param is the HTTP request.
     	* @return : HttpResponse The webservice call response.
    	*/    
    	 	
    public HttpResponse getPostSubscriptionNumber(HttpRequest request,  String responseType, Zuora.ZApi zApiInstance){
    	
    	string methodName = 'createSubscriptionNumber';
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName , '[request] :' + request);
        
     //   Zuora.ZApi zApiInstance = new Zuora.ZApi();
        HttpResponse response = null;

        if(!Test.isRunningTest()){
            response = zApiInstance.sendRequest(request);
            
            system.debug('ADBS DM002_Invoice ' + response.getBody());
             
            ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + response);  
        }else{
            response.setStatusCode(200);
        }
            
        return response ;       	
        
    }

    
    public static List<Zuora__ZInvoice__c> getListByInvoiceIds(Set<Id> InvoiceIds){
        
        String methodName = 'getListByInvoiceIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[InvoiceIds] :' + InvoiceIds);

        String soqlQuery = 'SELECT ' + ALL_FIELDS + ' FROM Zuora__ZInvoice__c WHERE Id IN: InvoiceIds';    
        List<Zuora__ZInvoice__c> listInvoices = new List<Zuora__ZInvoice__c>((List<Zuora__ZInvoice__c>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listInvoices);
        return listInvoices; 
    }
    
    public static List<Zuora__ZInvoice__c> getListByInvoiceIds(List<Id> InvoiceIds){
        
        String methodName = 'getListByInvoiceIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[InvoiceIds] :' + InvoiceIds);

        String soqlQuery = 'SELECT ' + ALL_FIELDS + ' FROM Zuora__ZInvoice__c WHERE Id IN: InvoiceIds';    
        List<Zuora__ZInvoice__c> listInvoices = new List<Zuora__ZInvoice__c>((List<Zuora__ZInvoice__c>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listInvoices);
        return listInvoices; 
    }
    
  
}