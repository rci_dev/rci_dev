/** 
* @author: Adolphe SONKO
* @date : Création 24/07/2017
* @date : Modification 24/07/2017
* @description : this Class allow to manage all database operations (such as select requests and dml) with Salesforce.com .
*/
public class DM001_Account {
	
    private static String CLASS_NAME = 'DM001_Account';
    private static final String ALL_FIELDS = ObjectUtil.constructFieldListForQueryFrom('Account');
    private static final String ALL_FIELDS2 = ObjectUtil.constructFieldListForQueryFrom('zqu__BillingEntity__c');
    
    
    /**
     * @author : Adolphe SONKO
     * @date : Création 24/07/2017
    * @date : Modification 24/07/2017 
     * @description : This method performs an SQL query on the account to repatriate its data from an ID list.
     * @param : Set<Id> AccountIds This parameter is a list of unique identifiers of account.
     * @return : List<Account> The returned item is a list of Accounts.
     */  
    public static List<Account> getListByIds(Set<Id> AccountIds){
        
        String methodName = 'getListByIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[AccountIds] :' + AccountIds);
        
        String soqlQuery = 'SELECT ' + ALL_FIELDS + ' FROM Account WHERE Id IN: AccountIds';    
        List<Account> listAccounts = new List<Account>((List<Account>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listAccounts);
        return listAccounts; 
    }
    
	/**
     * @author : Adolphe SONKO
     * @date : Création 24/07/2017
   	 * @date : Modification 24/07/2017 
     * @description : This method performs an SQL query on the Billing Entity to repatriate its data from an Account Country.
     * @param : Set<String> setAccountCountries This parameter is a list of Account countries.
     * @return : List<zqu__BillingEntity__c> The returned item is a list of Billing Entity.
     */
    public static List<zqu__BillingEntity__c> getListBillingEntitiesByCountries(Set<String> setAccountCountries){
        
        String methodName = 'getListBillingEntitiesByCountries';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[setAccountCountries] :' + setAccountCountries);
        
        String soqlQuery = 'SELECT ' + ALL_FIELDS2 + ' FROM zqu__BillingEntity__c WHERE zqu__EntityName__c IN: setAccountCountries';    
        List<zqu__BillingEntity__c> listBillingEntities = new List<zqu__BillingEntity__c>((List<zqu__BillingEntity__c>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listBillingEntities);
        return listBillingEntities; 
    }
    
    
   public static zqu__BillingEntity__c getListBillingEntitiesByCountry(String country){
        
        String methodName = 'getListBillingEntitiesByCountry';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[country] :' + country);
        
        String soqlQuery = 'SELECT ' + ALL_FIELDS2 + ' FROM zqu__BillingEntity__c WHERE zqu__EntityName__c =: country';    
        List<zqu__BillingEntity__c> listBillingEntities = new List<zqu__BillingEntity__c>((List<zqu__BillingEntity__c>)Database.query(soqlQuery));
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listBillingEntities);
       
        if(listBillingEntities != null && listBillingEntities.size() > 0){
               zqu__BillingEntity__c billingEntity = listBillingEntities.get(0);
               return billingEntity;
        }else{
               return null;
        }
       
    }
}