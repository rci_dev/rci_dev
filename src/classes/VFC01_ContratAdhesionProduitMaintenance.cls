public class VFC01_ContratAdhesionProduitMaintenance {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_ContratAdhesionProduitMaintenance
-- - Description   : fetch all details on the object Zuora__Subscription__c
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 16-JUN-2017  WCH   1.0      Initial version
------------------------------------------------------------------------------------------------
*/
    public String strToday {get;set;}
    public String currentSubsId {get;set;}
    public Zuora__Subscription__c currentSub{get;set;}
    public Zuora__SubscriptionProductCharge__c currentSubProd{get;set;}

    //DMU 20170703 - Added variable to store value of Zuora__SubscriptionProductCharge__c.Zuora__Price__c - 120
    //This requirement is wrt SC_DXC_RCI_UAT Return_V4.xsl
    public decimal zuoraPriceCalcualted {get;set;}

    public VFC01_ContratAdhesionProduitMaintenance() {
        currentSubsId = ApexPages.currentPage().getParameters().get('Id');
        try{
            currentSub = [SELECT   id
                            ,InsuredAccount__r.ContactFunctionalID__pc
                            ,InsuredAccount__r.FirstName
                            ,InsuredAccount__r.LastName
                            ,InsuredAccount__r.PersonEmail
                            ,InsuredAccount__r.PersonMailingPostalCode
                            ,InsuredAccount__r.PersonMobilePhone
                            ,Policy_Number__c
                            ,VehicleFirstRegistrationDate__c
                            ,VIN__c
                            ,SubscriberType__c
                            ,CompanyName__c
                            ,CompanyFunctionalID1__c
                            ,YearOfManufacture__c
                            ,Zuora__SubscriptionStartDate__c
                            ,InvoiceNumber__c
                            ,Zuora__Account__r.Gender__pc
                            ,PlateNumber__c
                            ,Brand__c
                            ,Model__c
                            ,Zuora__Account__r.BillingAddress
                            ,Zuora__Account__r.BillingCity
                            ,Zuora__Account__r.BillingPostalCode
                            ,Zuora__Account__r.BillingStreet
                            ,Zuora__Account__r.ContactFunctionalID__pc
                            ,Zuora__Account__r.PersonEmail
                            ,Zuora__Account__r.PersonMailingPostalCode
                            ,Zuora__Account__r.PersonMobilePhone
                            ,Zuora__Account__r.PersonBirthdate
                            ,Zuora__Account__r.BillingAddressLine1__c
                            ,Zuora__Account__r.BillingAddressLine2__c
                            ,Zuora__Account__r.BillingAddressLine3__c
                            ,Zuora__Account__r.BillingAddressLine4__c
                            ,Zuora__Account__r.BillingDistrict__c
                            ,Zuora__Account__r.BillingPostalCode__c
                            ,Zuora__Account__r.BillingCity__c
                            ,Zuora__TermStartDate__c 
                            ,Zuora__TermEndDate__c 
                            ,Zuora__OriginalCreated_Date__c
                            ,Zuora__SubscriptionNumber__c
                            ,Zuora__Account__r.LastName
                            ,Zuora__Account__r.FirstName
                            ,VehicleDeliveryDate__c
                            ,ModelYear__c 
                            ,Mileage__c
                            ,Zuora__Account__r.Phone
                            ,Zuora__CustomerAccount__r.Zuora__DefaultPaymentMethod__c
                            ,Zuora__InitialTerm__c
                            ,MileageAttribute__c
                            ,Zuora__Account__r.CompanyFunctionalID1__c
                            ,Zuora__Account__r.CompanyName__c
                            ,Zuora__Account__r.PersonHomePhone
                            ,LastCoveredDay__c
                   FROM Zuora__Subscription__c
                   WHERE id =:currentSubsId];

            strToday = Datetime.now().format('dd/MM/YY');
            currentSubProd = [select Id
                                ,Zuora__Price__c
                                ,Zuora__ProductName_Product__c
                                ,Zuora__BillingPeriod__c
                                FROM Zuora__SubscriptionProductCharge__c
                                where Zuora__Subscription__c= : currentSubsId 
                                limit 1];

            zuoraPriceCalcualted = currentSubProd.Zuora__Price__c != null ? currentSubProd.Zuora__Price__c - 120.00 : 0.00;

        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
        }
        
    }
}