@isTest(seeAllData=true)
private class AP01_SendSubscriptionEmail_TEST{
/*
------------------------------------------------------------------------------------------------
-- - Name          : AP01_SendSubscriptionEmail_TEST
-- - Description   : Test Class for AP01_SendSubscriptionEmail
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 24-JUN-2017  DMU   1.0      
-- 16-OCT-2017  NAK   1.1      Changement comportement adresses dynamiques  
------------------------------------------------------------------------------------------------*/   

    static User testUser;
    static list <Zuora__Subscription__c>  subscriptionList;
    static Account accSub;
    static Account accSubNoEmail;
    static list <Zuora__SubscriptionProductCharge__c> prodChargeList;
    
    static {
        system.debug('>>>>> START STATIC <<<<<');

        testUser = AP_Constant.createAdminUser('TestUserTriggerXYZ', AP_Constant.getProfileAdminId()); 
        testUser.BypassValidationRule__c = true;
        insert  testUser;

        System.runAs(testUser){

            //create account with mail 
            accSub = AP_Constant.createAccount('Test Account');
            accSub.PersonEmail = 'dhovisha.munbodh@sc-mauritius.com';
            accSub.RecordTypeId = AP_Constant.MapRecordType('Account').get('PersonAccountIndividual'); //'0120Y000000Vf5K';
            insert accSub;

            //create account without mail 
            accSubNoEmail = AP_Constant.createAccount('Test Account');
            accSubNoEmail.RecordTypeId = AP_Constant.MapRecordType('Account').get('PersonAccountIndividual');
            insert accSubNoEmail;   

            //select Folder 
            Id folderId = [SELECT id, DeveloperName, Name FROM Folder where DeveloperName = 'BSP_EmailTemplates'].Id;

            //create EmailTemplate
            list <EmailTemplate> emailTemplateList = new list <EmailTemplate> {new EmailTemplate(Name = 'EmailVoucher', DeveloperName = 'BSP_EmailVoucher1', Markup = '<messaging:emailTemplate subject="Prezent na powitanie - Voucher assistance" recipientType="User" relatedToType="Zuora__Subscription__c"> <messaging:htmlEmailBody > <c:C01_EmailVoucher subsId="{!relatedTo.Id}"/> </messaging:htmlEmailBody> </messaging:emailTemplate>'
                                                                            ,IsActive = true, FolderId = folderId, TemplateType = 'visualforce')
            };
            insert emailTemplateList;       

            /*NAK 18-10-2017 : Error Test Class  <<START>>
            //Map<String,SubscriptionEmailParam__c> OrgWideEmailAddressParams = SubscriptionEmailParam__c.getAll();
            
            //if(OrgWideEmailAddressParams.isEmpty()){

            //    List<OrgWideEmailAddress> lstOrgWideEmailAddress= [SELECT Id FROM OrgWideEmailAddress limit 1];

            //    //create custom setting records
            //    SubscriptionEmailParam__c EmailAddress = new SubscriptionEmailParam__c();
            //    EmailAddress.RecipientOrgWideName__c =  'Loja RCI Brasil';
            //    EmailAddress.Name = 'Default'; 
            //    //EmailAddress.Organization_Wide_Address_Id__c = '0D29E0000008PUH'; 
            //    EmailAddress.Organization_Wide_Address_Id__c = lstOrgWideEmailAddress[0].Id;  //NAK 16-10-2017 : Changement comportement adresses dynamiques 
            //    insert EmailAddress;
            }*/

            SubscriptionEmailParam__c subscriptionEmailParamDefault = SubscriptionEmailParam__c.getValues('Default');

            List<OrgWideEmailAddress> lstOrgWideEmailAddress = [SELECT Id FROM OrgWideEmailAddress WHERE displayname = 'Loja RCI Brasil' limit 1];

            if(subscriptionEmailParamDefault == null){
                //create custom setting records
                SubscriptionEmailParam__c EmailAddress = new SubscriptionEmailParam__c( RecipientOrgWideName__c =  'Loja RCI Brasil',
                                                                                        Name = 'Default',
                                                                                        Organization_Wide_Address_Id__c = lstOrgWideEmailAddress[0].Id);
                insert EmailAddress;
            }
            else{
                //update existing default custom setting record due to see all data
                subscriptionEmailParamDefault.Organization_Wide_Address_Id__c = lstOrgWideEmailAddress[0].Id;
                update subscriptionEmailParamDefault;
            }
             /*NAK 18-10-2017 : Error Test Class  <<END>>*/
        }
    }

    static testMethod void testInsertSubscriptionPL() {
        system.debug('>>>TEST METHOD testInsertSubscriptionPL<<<');        
        System.runAs(testUser){  
           
            Test.startTest();      

                Zuora__Subscription__c zSubPL = new Zuora__Subscription__c(Name = 'Sub2', SubscriptionCountry__c = 'PL', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id02', Zuora__Account__c = accSub.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = true, Zuora__TermEndDate__c = Date.today()  );
                insert zSubPL;

                Zuora__Subscription__c zSubPLCheck = [select id, Tech_EmailNotificationVoucher__c from Zuora__Subscription__c where id =: zSubPL.Id];
                system.assertEquals(true, zSubPLCheck.Tech_EmailNotificationVoucher__c);
            Test.stopTest();    
        }
    }
    
    static testMethod void testUpdateSubscription() {
        system.debug('>>>TEST METHOD testUpdateSubscription<<<');        
        System.runAs(testUser){  
            Zuora__Subscription__c zSubCon = new Zuora__Subscription__c(Name = 'Sub2', SubscriptionCountry__c = 'UK', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id02', Zuora__Account__c = accSub.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = false, TECH_COUNT_ProductCharges__c = 1, TECH_COUNT_RatePlan__c = 1, Zuora__TermEndDate__c = Date.today()  );
            insert zSubCon;
            Zuora__SubscriptionProductCharge__c zSubProductCharge = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = zSubCon.Id, Name='pc1_1', Zuora__Zuora_Id__c = 'id01', Zuora__ProductName__c = 'Concierge Club' );
            insert zSubProductCharge;
            Zuora__SubscriptionRatePlan__c zSubRatePlan = new Zuora__SubscriptionRatePlan__c(Zuora__Subscription__c = zSubCon.Id, Name = system.label.ConciergeSilver, Zuora__SubscriptionRatePlanName__c = system.label.ConciergeSilver, CurrencyIsoCode = 'EUR');
            insert zSubRatePlan;
            zSubCon.Mileage__c = 25;
            Test.startTest();                
                update zSubCon;
                Zuora__Subscription__c zSubConCheck = [select id, Tech_EmailNotification__c from Zuora__Subscription__c where id =: zSubCon.Id];
                system.assertEquals(true, zSubConCheck.Tech_EmailNotification__c);
            Test.stopTest();    
        }
    }

    static testMethod void testUpdateSubscriptionBulk() {
        system.debug('>>>TEST METHOD testUpdateSubscriptionBulk<<<');        
        System.runAs(testUser){  
            subscriptionList = new list <Zuora__Subscription__c> { new Zuora__Subscription__c(Name = 'Sub0', SubscriptionCountry__c = 'UK', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id00', Zuora__Account__c = accSub.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = false, TECH_COUNT_ProductCharges__c = 1, TECH_COUNT_RatePlan__c = 1, Zuora__TermEndDate__c = Date.today() )
                                                                    ,new Zuora__Subscription__c(Name = 'Sub1', SubscriptionCountry__c = 'UK', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id01', Zuora__Account__c = accSub.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = false, TECH_COUNT_ProductCharges__c = 1, TECH_COUNT_RatePlan__c = 1, Zuora__TermEndDate__c = Date.today() )
                                                                    ,new Zuora__Subscription__c(Name = 'Sub2', SubscriptionCountry__c = 'UK', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id02', Zuora__Account__c = accSub.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = false, TECH_COUNT_ProductCharges__c = 1, TECH_COUNT_RatePlan__c = 1, Zuora__TermEndDate__c = Date.today() )
                                                                    ,new Zuora__Subscription__c(Name = 'Sub3', SubscriptionCountry__c = 'UK', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id02', Zuora__Account__c = accSub.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = false, TECH_COUNT_ProductCharges__c = 1, TECH_COUNT_RatePlan__c = 1, Zuora__TermEndDate__c = Date.today() )
            };
            insert subscriptionList;

            list <Zuora__SubscriptionProductCharge__c> zSubProductChargelist = new list <Zuora__SubscriptionProductCharge__c> {new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = subscriptionList[0].Id, Name='pc1_1', Zuora__Zuora_Id__c = 'id01', Zuora__ProductName__c = 'Cyber Risk' )
                                                                                                                                ,new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = subscriptionList[1].Id, Name='pc1_1', Zuora__Zuora_Id__c = 'id01', Zuora__ProductName__c = 'SecurPlus' )
                                                                                                                                ,new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = subscriptionList[2].Id, Name='pc1_1', Zuora__Zuora_Id__c = 'id01', Zuora__ProductName__c = 'Key Cover' )
                                                                                                                                ,new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = subscriptionList[3].Id, Name='pc1_1', Zuora__Zuora_Id__c = 'id01', Zuora__ProductName__c = 'Maintenance' )
            };
            insert zSubProductChargelist;
            
            Test.startTest();   
                subscriptionList[0].Mileage__c  = 25;       
                subscriptionList[1].Mileage__c  = 25;        
                subscriptionList[2].Mileage__c  = 25;     
                subscriptionList[3].Mileage__c  = 25;        
                update subscriptionList;
                list <Zuora__Subscription__c> zSubCheckList = [select id, Tech_EmailNotification__c from Zuora__Subscription__c where id IN: subscriptionList];
                system.assertEquals(true, zSubCheckList[0].Tech_EmailNotification__c);
                system.assertEquals(true, zSubCheckList[1].Tech_EmailNotification__c);
                system.assertEquals(true, zSubCheckList[2].Tech_EmailNotification__c);
                system.assertEquals(true, zSubCheckList[3].Tech_EmailNotification__c);
            Test.stopTest();    
        }
    }

    static testMethod void testSubscriptionNoEmail() {
        system.debug('>>>TEST METHOD testSubscriptionNoEmail<<<');        
        System.runAs(testUser){  
            Zuora__Subscription__c zSubCon = new Zuora__Subscription__c(Name = 'Sub2', SubscriptionCountry__c = 'UK', CurrencyIsoCode = 'EUR', Zuora__Zuora_Id__c = 'id02', Zuora__Account__c = accSubNoEmail.Id, Tech_SyncCommerceCloud__c = true, Policy_Number__c = 'P01', VoucherNumber_PL__c = 'V01', Tech_EmailNotification__c = false, TECH_COUNT_ProductCharges__c = 1, TECH_COUNT_RatePlan__c = 1, Zuora__TermEndDate__c = Date.today());
            insert zSubCon;
            Zuora__SubscriptionProductCharge__c zSubProductCharge = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = zSubCon.Id, Name='pc1_1', Zuora__Zuora_Id__c = 'id01', Zuora__ProductName__c = 'Concierge Club' );
            insert zSubProductCharge;
            Zuora__SubscriptionRatePlan__c zSubRatePlan = new Zuora__SubscriptionRatePlan__c(Zuora__Subscription__c = zSubCon.Id, Name = system.label.ConciergeSilver, Zuora__SubscriptionRatePlanName__c = system.label.ConciergeSilver, CurrencyIsoCode = 'EUR');
            insert zSubRatePlan;
            zSubCon.Mileage__c = 25;
            Test.startTest();   
                try{
                    update zSubCon;
                }catch(exception e){
                    system.debug('### enter catch');
                    Zuora__Subscription__c zSubConCheck = [select id, Tech_EmailNotification__c from Zuora__Subscription__c where id =: zSubCon.Id];
                    system.assertEquals(false, zSubConCheck.Tech_EmailNotification__c);
                }                    
            Test.stopTest();    
        }
    }

}