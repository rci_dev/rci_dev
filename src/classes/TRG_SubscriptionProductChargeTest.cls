/**
* @author BGU
* @date Creation 25/07/2017
* @date Modification 25/07/2017
* @description : TRG_SubscriptionProductCharge Unit Test Class 
*/


@isTest
public class TRG_SubscriptionProductChargeTest {

    private static User userTest = TestFactory.getStandardUser();
    
    
    static testMethod void updateBSPFieldsTest(){
        
        
        //Insert Custom settings
        IdPolicyNumber__c param = new IdPolicyNumber__c(
            Name='Policy',
   		Concierge__c =0, 
   		Cyber__c=0,
   		Tyre__c=0);
        
        
        IdVoucherNumber__c param2 = new IdVoucherNumber__c(
            Name='Voucher',
        Voucher_Number__c= 0,
            Voucher_Number_Max__c=500
        
        );          
        
        
        
        
        // Test Begin
        System.test.startTest(); 
        
        System.runAs(userTest){
            
            insert param;
         	 insert param2;
            Zuora__Subscription__c test1 = TestFactory.getSimpleZuoraSubscription();
            insert test1;
            
            Zuora__Subscription__c test2 = TestFactory.getSimpleZuoraSubscription();
            insert test2;
            
            Zuora__Subscription__c test3 = TestFactory.getSimpleZuoraSubscription();
            insert test3;
            
            Zuora__Subscription__c test4 = TestFactory.getSimpleZuoraSubscription();
            insert test4;
            
            Zuora__SubscriptionProductCharge__c testFinal = TestFactory.getSimpleZuoraSubscriptionProductCharge('Concierge Club','PL-00000006',test1);
            
            Zuora__SubscriptionProductCharge__c testFinal2 = TestFactory.getSimpleZuoraSubscriptionProductCharge('Tyre Insurance','PL-00000003',test2);
            
            Zuora__SubscriptionProductCharge__c testFinal3 = TestFactory.getSimpleZuoraSubscriptionProductCharge('Cyber Risk','PL-00000001',test3);
            
            Zuora__SubscriptionProductCharge__c testFinal4 = TestFactory.getSimpleZuoraSubscriptionProductCharge('Travel Insurance','PL-00000007',test4);
            insert testFinal;
            insert testFinal2;
            insert testFinal3;
            insert testFinal4;
             
        
        
        }
        
        System.test.stopTest();
        
    }
    
    
}