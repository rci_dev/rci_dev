public without sharing class AP01_SendSubscriptionEmail {
/*
------------------------------------------------------------------------------------------------
-- - Name          : AP01_SendSubscriptionEmail
-- - Description   : construct and send email
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 19-JUN-2017  WCH   1.0      Initial version
-- 24-JUN-2017  DMU   1.1      Added conditions to cater for CYBERISK/Maintenance/SecurPlus/KeyProtection
                               and CONCIERGE_SILVER/ CONCIERGE_GOLD/ CYBERRISK/ Maintenance/ SECURPLUS/ Key Protection 
-- 10-Aug-2017  DMU   1.2      Modified theTargetObjectId to take contactId                        
-- 19-Sep-2017  WCH   1.3      Dynamisme de l’envoie d’email                       
-- 10-OCT-2017  MGR   1.4      Changement pour Static et Dynamic Doc Name : Pologne       
-- 16-OCT-2017  NAK   1.5      Changement comportement adresses dynamiques     
------------------------------------------------------------------------------------------------*/    
    private static List<Zuora__Subscription__c> updZuoraSubs; 
    private static Map<Id, String> zSubsIdMapPlanName;
    private static Map<Id, String> accountIdMapPersonEmail;    
    public static Boolean boolRunOnce=false;    
    private static Map<String, Id> emailTempMap;
    public static List<Zuora__SubscriptionProductCharge__c> zProdChargeList;
    private static Map<Id, String> zProdChargeMap;    
    public static Boolean isMailVoucherSent=false;   
    public static List<Contact> conList;

    /*******************************************************************************************************************************************
    * function : sendTyreInsuranceMail > send mail for pneus maxi/mini and 
    CYBERISK/Maintenance/SecurPlus/KeyProtection/CONCIERGE_SILVER/ CONCIERGE_GOLD/ CYBERRISK/ Maintenance/ SECURPLUS/ Key Protection 
    ********************************************************************************************************************************************/
    
    public static void sendTyreInsuranceMail(Map<Id, Zuora__Subscription__c> ZuoraSubLst,Map<Id, Id> zuoraSubNumMapZuoraAccount){

        boolRunOnce=true;
        
        system.debug('AP01_SendSubscriptionEmail >>sendTyreInsuranceMail >>ZuoraSubLst >>'+ZuoraSubLst);
        system.debug('AP01_SendSubscriptionEmail >>sendTyreInsuranceMail >>zuoraSubNumMapZuoraAccount >>'+zuoraSubNumMapZuoraAccount);        
        
        updZuoraSubs=new List<Zuora__Subscription__c>(); 
        zSubsIdMapPlanName = new Map<Id, String>();
        accountIdMapPersonEmail = new Map<Id, String>();        
        emailTempMap = new Map<String, Id>();
        zProdChargeMap = new Map<Id, String>();
        //retrieving all org wide address params
        Map<String,SubscriptionEmailParam__c> OrgWideEmailAddressParams = SubscriptionEmailParam__c.getAll();

        //DMU: 20170810 - Added to cater for correction of mail send/receive        
        conList = new list <Contact>();                
        
        for(EmailTemplate eT : [Select id, DeveloperName 
                                from EmailTemplate 
                                where DeveloperName LIKE 'BSP_%' ]){ 
            emailTempMap.put(eT.DeveloperName, eT.Id);
        }

        system.debug('### emailTempMap: '+emailTempMap);

        List<Zuora__SubscriptionRatePlan__c> zSubsRatePlan = [select Id,Zuora__Subscription__c,Zuora__SubscriptionRatePlanName__c FROM Zuora__SubscriptionRatePlan__c where Zuora__Subscription__c IN: ZuoraSubLst.keySet()];
        system.debug('AP01_SendSubscriptionEmail >>sendTyreInsuranceMail >>zSubsRatePlan >>'+zSubsRatePlan);
        List<Account> zAccounts = [select Id,personemail FROM Account where Id IN: zuoraSubNumMapZuoraAccount.values()];
        system.debug('AP01_SendSubscriptionEmail >>sendTyreInsuranceMail >>zAccounts >>'+zAccounts);

        for(Account currentAcc : zAccounts){
            accountIdMapPersonEmail.put(currentAcc.Id,currentAcc.personemail);
        }

        for (Contact con : [select accountId,name from contact where accountId IN :zAccounts limit 1]){
            conList.add(con);
        }
        system.debug('#### conList: '+ conList);  

        Id theTargetObjectId = conList[0].Id;  //conList always contains 1 contact
        system.debug('### user from Email Add = '+theTargetObjectId);
        
        for(Zuora__SubscriptionRatePlan__c currentsubRatePlan : zSubsRatePlan){
            if(ZuoraSubLst.containsKey(currentsubRatePlan.Zuora__Subscription__c)){
                zSubsIdMapPlanName.put(currentsubRatePlan.Zuora__Subscription__c,currentsubRatePlan.Zuora__SubscriptionRatePlanName__c);
            }
        }
        system.debug('#### zSubsIdMapPlanName: '+zSubsIdMapPlanName.size());
        system.debug('#### ZuoraSubLst.values(): '+ZuoraSubLst.values());        

        //Added to cater for CYBERISK/Maintenance/SecurPlus/KeyProtection - condition zuora__subscription_rate_plans__r.zuora__subscriptionrateplanname__c
        zProdChargeList = [Select id, Zuora__Subscription__c, zuora__productname_product__c from Zuora__SubscriptionProductCharge__c where Zuora__Subscription__c IN: ZuoraSubLst.keySet()];
        for (Zuora__SubscriptionProductCharge__c zpc : zProdChargeList){
            if(ZuoraSubLst.containsKey(zpc.Zuora__Subscription__c)){
                zProdChargeMap.put(zpc.Zuora__Subscription__c, zpc.zuora__productname_product__c);
            }
        }
        system.debug('#### zProdChargeMap size: '+zProdChargeMap.size());
        
        for(Zuora__Subscription__c theSubs : ZuoraSubLst.values()){
            //NAK 16-10-2017 : Changement comportement adresses dynamiques 
            //String orgWideAddName = OrgWideEmailAddressParams.get(theSubs.WebsiteSource__c)!=null ? OrgWideEmailAddressParams.get(theSubs.WebsiteSource__c).RecipientOrgWideName__c : OrgWideEmailAddressParams.get(AP_Constant.OrgWideDefault).RecipientOrgWideName__c;
            String orgWideAddName = OrgWideEmailAddressParams.get(theSubs.WebsiteSource__c)!=null ? OrgWideEmailAddressParams.get(theSubs.WebsiteSource__c).Organization_Wide_Address_Id__c : OrgWideEmailAddressParams.get(AP_Constant.OrgWideDefault).Organization_Wide_Address_Id__c;

            system.debug('#### theSubs.Id: '+theSubs.Id);
            if (accountIdMapPersonEmail.get(theSubs.Zuora__Account__c) != null){

                //Voucher 
                if(theSubs.Tech_EmailNotificationVoucher__c == False){
                    if (theSubs.SubscriptionCountry__c == 'PL'){
                        createEmail(emailTempMap.get('BSP_EmailVoucher'),theTargetObjectId,theSubs.Id ,'EmailVoucher',orgWideAddName);
                    }
                }

                //  PneuMini/ PneuMaxi/ CONCIERGE_SILVER/ CONCIERGE_GOLD/ CYBERRISK/ Maintenance/ SECURPLUS/ Key Protection 
                if(theSubs.Tech_EmailNotification__c == False){
                    if(zSubsIdMapPlanName.get(theSubs.Id) != null){
                        SysteM.debug(' ## NAK ' + zSubsIdMapPlanName.get(theSubs.Id));
                        //PneuMini
                        if(zSubsIdMapPlanName.get(theSubs.Id).equals(system.label.TyreInsuranceMINI)){
                            createEmail(emailTempMap.get('BSP_EmailTemplateGenericPneu'),theTargetObjectId,theSubs.Id ,'PneusMini',orgWideAddName);
                        }
                        //PneuMaxi
                        if(zSubsIdMapPlanName.get(theSubs.Id).equals(system.label.TyreInsuranceMAXI)){
                            createEmail(emailTempMap.get('BSP_EmailTemplateGenericPneu'),theTargetObjectId,theSubs.Id ,'PneusMaxi',orgWideAddName);
                        }
                        //CONCIERGE_SILVER
                        if(zSubsIdMapPlanName.get(theSubs.Id).equals(system.label.ConciergeSilver)){
                            createEmail(emailTempMap.get('BSP_EmailTemplateGenericConcierge'),theTargetObjectId,theSubs.Id ,'ConciergeSilver',orgWideAddName);  
                        }
                        //CONCIERGE_GOLD
                        if(zSubsIdMapPlanName.get(theSubs.Id).equals(system.label.ConciergeGold)){
                            createEmail(emailTempMap.get('BSP_EmailTemplateGenericConcierge'),theTargetObjectId,theSubs.Id ,'ConciergeGold',orgWideAddName);  
                        }
                    }

                    if(string.valueof(zProdChargeMap.get(theSubs.Id)) != null){
                        //CYBERRISK
                        if(string.valueof(zProdChargeMap.get(theSubs.Id)).contains(system.label.CYBERRISK) == true){
                            createEmail(emailTempMap.get('BSP_EmailTemplateGenericCyberrisk'),theTargetObjectId,theSubs.Id ,'Cyberrisk',orgWideAddName);
                        }
                        //Maintenance 
                        if(string.valueof(zProdChargeMap.get(theSubs.Id)).contains(system.label.Maintenance) == true){
                            createEmail(emailTempMap.get('BSP_EmailMaintenance'),theTargetObjectId,theSubs.Id ,'Maintenance',orgWideAddName);
                        }
                        //SECURPLUS
                        if(string.valueof(zProdChargeMap.get(theSubs.Id)).contains(system.label.SecurPlus) == true){
                            createEmail(emailTempMap.get('BSP_EmailSecurPlus'),theTargetObjectId,theSubs.Id ,'SecurPlus',orgWideAddName);
                        }
                        //Key Protection 
                        if(string.valueof(zProdChargeMap.get(theSubs.Id)).contains(system.label.KeyCover) == true){
                            createEmail(emailTempMap.get('BSP_EmailKeyProtection'),theTargetObjectId,theSubs.Id ,'KeyProtection',orgWideAddName);
                        }
                    }
                }
            }else{
                theSubs.addError(system.label.EmailDocMissingAddress);
            }

        }
        for(Zuora__Subscription__c currentSub : [select Id,Tech_EmailNotification__c, Tech_EmailNotificationVoucher__c, SubscriptionCountry__c FROM Zuora__Subscription__c where Id IN : ZuoraSubLst.keyset()]){
            currentSub.Tech_EmailNotification__c = true;  
            currentSub.Tech_EmailNotificationVoucher__c = currentSub.SubscriptionCountry__c == 'PL' ? true : false;            
            updZuoraSubs.add(currentSub);   
        }
        update updZuoraSubs; 
    }// end sendTyreInsuranceMail


    @future(callout=true)
    public static void createEmail(Id theTemplateId, Id theTargetObjectId, Id whatId, String whichEmailToGenerate,String orgWideEmailAddressName){
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> ();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List <String> emailRecipients = new List<String>();
        
        mail.setTemplateID(theTemplateId); 
        mail.setBccSender(false);
        mail.setUseSignature(false);

        //DMU: 20170810 - This theTargetObjectId takes the contactId Subscription.account since Subscription.account is a personAccount
        mail.setTargetObjectId(theTargetObjectId); 
        
        //Id theOrgWideEmailAddressId = [SELECT DisplayName,Id FROM OrgWideEmailAddress where DisplayName =: system.label.AP01OrgWideAddDisplayName].Id;
        //NAK 16-10-2017 : Changement comportement adresses dynamiques <<START>> 
        /*list <OrgWideEmailAddress> orgWideIdList = [SELECT DisplayName, 
                                                           Id
                                                    FROM OrgWideEmailAddress 
                                                    WHERE DisplayName =: orgWideEmailAddressName 
                                                    LIMIT 1];   
        if (orgWideIdList.size()>0){
            mail.setOrgWideEmailAddressId(orgWideIdList[0].Id);
        }*/
        mail.setOrgWideEmailAddressId((Id)orgWideEmailAddressName);
        //NAK 16-10-2017 : Changement comportement adresses dynamiques <<END>> 
        mail.setWhatId(whatId);
        mail.saveAsActivity =false;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();

        String dynamicDocName = null;
        String staticDocName = null;

        Blob b = null;
        if (!whichEmailToGenerate.equals('KeyProtection')) {    //This condition is because only KeyProtection do not send dynamic email
            system.debug('### enter whichEmailToGenerate = '+ whichEmailToGenerate);

            //Create Dynamic Attachment
            PageReference pageref = null;           

            pageref = (whichEmailToGenerate.equals('PneusMini')) ? Page.VFP01_PneusMiniPolicies : (whichEmailToGenerate.equals('PneusMaxi') ? Page.VFP01_PneusMaxiPolicies : (whichEmailToGenerate.equals('EmailVoucher') ? Page.VFP01_Voucher : (whichEmailToGenerate.equals('Cyberrisk') ? Page.VFP01_CyberRiskPolices : (whichEmailToGenerate.equals('ConciergeSilver') ? Page.VFP01_ConciergePolicies : (whichEmailToGenerate.equals('ConciergeGold') ? Page.VFP01_ConciergePolicies : (whichEmailToGenerate.equals('Maintenance') ? Page.VFP01_ContratAdhesionProduitMaintenance : (Page.VFP01_WelcomeLetterSecurplus)))))));
            pageref.getParameters().put('Id',whatId);
            pageref.setRedirect(true);
            b = Test.isRunningTest() == false ? pageref.getContent() : blob.valueof('dummyValue');   

            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            
            //set dynamic attachment name
            dynamicDocName = (whichEmailToGenerate.equals('PneusMini')) ? system.label.EmailDynamicDocPneuMini : (whichEmailToGenerate.equals('PneusMaxi') ? system.label.EmailDynamicDocPneuMaxi : (whichEmailToGenerate.equals('EmailVoucher') ? system.label.EmailDynamicDocVoucher : (whichEmailToGenerate.equals('Cyberrisk') ? system.label.EmailDynamicDocCyberrisk : (whichEmailToGenerate.equals('ConciergeSilver') ? system.label.EmailDynamicDocConciergeSil : (whichEmailToGenerate.equals('ConciergeGold') ? system.label.EmailDynamicDocConciergeGO : (whichEmailToGenerate.equals('Maintenance') ? system.label.EmailDynamicDocMaintenance : system.label.EmailDynamicDocSecurPlus))))));

            efa.setFileName(dynamicDocName);         
            efa.setBody(b); 

            fileAttachments.add(efa);
        } 

        //Create Static Attachment
        Blob staticDocBlob = null;
        if (!whichEmailToGenerate.equals('EmailVoucher')){    //This condition is because only EmailVoucher do not send static doc

            List<StaticResource> objPDF1 = [Select body, name from StaticResource where Name Like 'OWU_%' order by Name asc];
            system.debug('### objPDF1: '+objPDF1); 

            staticDocBlob = (whichEmailToGenerate.equals('PneusMini') || whichEmailToGenerate.equals('PneusMaxi')) ? objPDF1[0].Body : (whichEmailToGenerate.equals('Cyberrisk') ? objPDF1[1].Body : (whichEmailToGenerate.equals('ConciergeSilver') || whichEmailToGenerate.equals('ConciergeGold') ? objPDF1[2].Body : (whichEmailToGenerate.equals('Maintenance') ? objPDF1[3].Body : (whichEmailToGenerate.equals('SecurPlus') ? objPDF1[4].Body : objPDF1[5].Body))));
            
            //staticDocName =  (whichEmailToGenerate.equals('PneusMini') || whichEmailToGenerate.equals('PneusMaxi')) ? system.label.EmailDocPneu : (whichEmailToGenerate.equals('Cyberrisk') ? system.label.EmailDocCyberrisk : (whichEmailToGenerate.equals('ConciergeSilver') || whichEmailToGenerate.equals('ConciergeGold') ? system.label.EmailDocConcierge : (whichEmailToGenerate.equals('Maintenance') ? system.label.EmailDocMaintenance : (whichEmailToGenerate.equals('SecurPlus') ? system.label.EmailDocSecurPlus : system.label.EmailDocKeyProtection))));

            
            //MGR 10-10-2017 :350223 <<START>> ConciergeSilver and ConciergeGold separated
            staticDocName =  (whichEmailToGenerate.equals('PneusMini') || whichEmailToGenerate.equals('PneusMaxi')) ? system.label.EmailDocPneu : 
                            (whichEmailToGenerate.equals('Cyberrisk') ? system.label.EmailDocCyberrisk : 
                            (whichEmailToGenerate.equals('ConciergeSilver') ? system.label.EmailDocConciergeSilver :
                            (whichEmailToGenerate.equals('ConciergeGold') ? system.label.EmailDocConciergeGold : 
                            (whichEmailToGenerate.equals('Maintenance') ? system.label.EmailDocMaintenance : 
                            (whichEmailToGenerate.equals('SecurPlus') ? system.label.EmailDocSecurPlus : system.label.EmailDocKeyProtection)))));
            //MGR 10-10-2017 :350223 <<START>>
            
            Messaging.EmailFileAttachment objPDFAttachment = new Messaging.EmailFileAttachment();            
            objPDFAttachment.setFileName(staticDocName);
            objPDFAttachment.setBody(staticDocBlob);     
            
            fileAttachments.add(objPDFAttachment);
        }

        mail.setFileAttachments(fileAttachments);
        messages.add(mail);        
       
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages); 
        system.debug('##### results: '+results);          
        if (results[0].success) {
            System.debug('The email was sent successfully.'); 
            system.debug('##### messages: '+messages);                
        } else {
            System.debug('The email failed to send: '+ results[0].errors[0].message);            
        }        
        
        //Insert EmailMessage
        EmailTemplate eT = [Select id, DeveloperName, body, HtmlValue, Subject from EmailTemplate where DeveloperName LIKE 'BSP_%' and id =: theTemplateId ];       
        System.debug('The email html body: '+ eT.HtmlValue); 
        string emailbodyPlainText = eT.HtmlValue != null ? (eT.HtmlValue).stripHtmlTags() : ' ';           
        System.debug('The email plaintext body: '+ emailbodyPlainText); 
        Zuora__Subscription__c sub = [Select Zuora__Account__c, Zuora__Account__r.personEmail from Zuora__Subscription__c where id =: whatId ];

        //NAK 16-10-2017 : Update for FromName + From Add
        OrgWideEmailAddress orgWideAdd = [SELECT DisplayName
                                                ,Id
                                                ,Address
                                        FROM OrgWideEmailAddress 
                                        WHERE Id =: orgWideEmailAddressName];  
             
        EmailMessage em4 = new EmailMessage(FromName = orgWideAdd.DisplayName,FromAddress = orgWideAdd.Address ,ToAddress = sub.Zuora__Account__r.personEmail, Subject = eT.Subject,TextBody = emailbodyPlainText,RelatedToId = whatId,MessageDate = system.Today(),Status = '3', Account__c = sub.Zuora__Account__c );   
        insert em4;

        list <Attachment> attachmentList = new list <Attachment>{new Attachment(ParentId = em4.Id ,Name = dynamicDocName,ContentType = 'application/pdf',Body = b)
                                                                ,new Attachment(ParentId = em4.Id ,Name = staticDocName,ContentType = 'application/pdf',Body = staticDocBlob)
        };

        list <Attachment> attachmentListVoucher = new list <Attachment>{new Attachment(ParentId = em4.Id ,Name = dynamicDocName,ContentType = 'application/pdf',Body = b)                                                            
        };

        list <Attachment> attachmentListKeyProtection = new list <Attachment>{new Attachment(ParentId = em4.Id ,Name = staticDocName,ContentType = 'application/pdf',Body = staticDocBlob)                                                               
        };

        if (whichEmailToGenerate.equals('EmailVoucher')){  
            insert attachmentListVoucher;
        }else if (whichEmailToGenerate.equals('KeyProtection')){
            insert attachmentListKeyProtection;
        }else{
            insert attachmentList;
        }
    }

}