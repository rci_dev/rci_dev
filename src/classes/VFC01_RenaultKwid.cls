public class VFC01_RenaultKwid {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_RenaultKwid
-- - Description   : Controller for the class VFP01_RenaultKwid
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 18-JUL-2017  NAK   1.0      Initial version
------------------------------------------------------------------------------------------------
*/

    public Id currentSubsId {get;set;}
    public Zuora__Subscription__c currentSub{get;set;}
    public Zuora__SubscriptionProductCharge__c currentProductCharge{get;set;}
    public String strToday {get;set;}

    public VFC01_RenaultKwid(){
        currentSubsId = ApexPages.currentPage().getParameters().get('Id');

         try{
            currentSub = [SELECT   id
                            ,Brand__c
                            ,SubscriberType__c
                            ,CompanyName__c
                            ,CompanyFunctionalID1__c
                            ,Zuora__Account__r.FirstName
                            ,Zuora__Account__r.LastName
                            ,Zuora__Account__r.ContactFunctionalID__pc
                            ,Zuora__Account__r.Citizenship__pc
                            ,Model__c
                            ,ModelYear__c
                            ,VIN__c
                            ,Zuora__Account__r.MailingCity__c
                            ,Zuora__Account__r.BillingAddressLine4__c 
                            ,Zuora__Account__r.BillingAddressLine3__c 
                            ,Zuora__Account__r.BillingAddressLine2__c 
                            ,Zuora__Account__r.BillingAddressLine1__c
                            ,Zuora__Account__r.BillingAddress
                            ,Zuora__Account__r.BillingCity
                            ,Zuora__Account__r.BillingPostalCode
                            ,Zuora__Account__r.BillingStreet
                            ,Zuora__Account__r.PersonBirthdate
                            ,Zuora__Account__r.PersonEmail
                            ,Zuora__Account__r.PersonMailingPostalCode
                            ,Zuora__Account__r.PersonMobilePhone
                            ,Zuora__Account__r.BillingCity__c 
                            ,Zuora__Account__r.BillingDistrict__c 
                            ,Zuora__Account__r.BillingCountry__c 
                            ,Zuora__Account__r.BillingPostalCode__c 
                            //,zuora__subscription_product_charges__r.Zuora__Price__c 

                   FROM Zuora__Subscription__c
                   WHERE id = :currentSubsId];

            currentProductCharge = [SELECT Zuora__Price__c 
                                    FROM Zuora__SubscriptionProductCharge__c
                                    WHERE Zuora__Subscription__c  = :currentSubsId
                                    LIMIT 1];

            strToday = Datetime.now().format('dd/MM/YY');
        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
        }
    }





}