/** 
* @author: Adolphe Sonko
* @date : Création 13/06/2017
* @date : Modification 31/07/2017
* @description : This Class allows to handle trigger events in Account entity.
*/
public class SM001_AccountTriggerHandler {
  
    private static String  CLASS_NAME = SM001_AccountTriggerHandler.class.getName();
    
    /**
   * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method 
     * @param : List<Account> accounts This parameter is the new triggered accounts.
     * @return : N/A.
     */
    public static void onAfterInsert(List<Account> accounts){
        
        String methodName = 'onAfterInsert';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [accounts] : ' + accounts);
        
            for(Account a : accounts){
                // Check if zuora Id is not empty at insertion
                if(!ObjectUtil.isEmpty(a.Tech_ZuoraAccountNumber__c)){
                    
                    // Begin the callout process
                    callUpdateZuoraAccount(a.Tech_ZuoraAccountNumber__c, a.Id, a.AccountCountry__c); 
                }         
        	}  
 
    }
    
    /**
   * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method 
     * @param : List<Account> accounts This parameter is the new triggered accounts.
     * @return : N/A.
     */
    public static void onAfterUpdate(List<Account> accounts, Map<Id, Account> oldAccounts){
        
        String methodName = 'onAfterUpdate';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [accounts] : ' + accounts);
        
            for(Account a : accounts){
                // Check if zuora Id is not empty and updated
                 if(!ObjectUtil.isEmpty(a.Tech_ZuoraAccountNumber__c) 
                   && (a.Tech_ZuoraAccountNumber__c!= oldAccounts.get(a.Id).Tech_ZuoraAccountNumber__c)){
                    
                    // Begin the callout process
                    callUpdateZuoraAccount(a.Tech_ZuoraAccountNumber__c, a.Id, a.AccountCountry__c); 
                }           
            }  
    }
    
    /**
     * @author: Adolphe Sonko
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method 
     * @param : string ZuoraAccountId This parameter is the Zuora ID of the new triggered account for callout.
     * @param : Id accountId This parameter is the Salesforce ID of the new triggered account for callout.
     * @return : N/A.
     */
    @future(callout=true)
    public static void callUpdateZuoraAccount(string ZuoraAccountNumber, Id accountId, string country){
        
        String methodName = 'callUpdateZuoraAccount';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [ZuoraAccountNumber] : ' + ZuoraAccountNumber);
        List<Zuora.zObject> zobjs = new List<Zuora.zObject>();
        List<Zuora.zObject> objs = new List<Zuora.zObject>();
        Zuora.zApi zApiInstance = new Zuora.zApi();

        zqu__BillingEntity__c billingEntity = EM001_Account.getListBillingEntitiesByCountry(country);
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [billingEntity] : ' + billingEntity);

        if(billingEntity != null){
   
            // Connexion to Zuora instance
            if(!Test.isRunningTest()){
                if (    (billingEntity.zqu__EntityName__c == 'BR')  
                      ||(billingEntity.zqu__EntityName__c == 'DE')
                      ||(billingEntity.zqu__EntityName__c == 'UK')
                      ||(billingEntity.zqu__EntityName__c == 'PL')
                    ) {
                        zApiInstance.zlogin(billingEntity.zqu__EntityID__c);
                    } 
                else{
                        zApiInstance.zlogin();
                    }   
            }

             
            ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [zApiInstance] : ' + zApiInstance);
            // Retrieve the Zuora account    
            String zoql = 'SELECT Id, Name from Account Where AccountNumber =\''+ ZuoraAccountNumber +'\'';
            
            if(!Test.isRunningTest()){
                zobjs = zApiInstance.zquery(zoql);
            }else{
                Zuora.zObject accUpdate = new Zuora.zObject('Account');
                zobjs.add(accUpdate);
            }
            
            ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [query result] : ' + zobjs.size());
            ObjectUtil.debugLogBegin(CLASS_NAME, methodName, ' [qzobjs] : ' + zobjs);
            
            for (Zuora.zObject o : zobjs) {
                // Update the account in Zuora
                o.setValue('CrmId', accountId);
                objs.add(o);
            }
                

            if(!Test.isRunningTest()){
                if(objs.size() > 0){
                    List<Zuora.zApi.SaveResult> results = zApiInstance.zupdate(objs); 
                }  
            }
        }
    }


}