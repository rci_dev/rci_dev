public class AP01_SendRSIEmail {
/*
------------------------------------------------------------------------------------------------
-- - Name          : AP01_SendRSIEmail
-- - Description   : construct and send email
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 19-JUL-2017  NAK   1.0      Initial versionn 
-- 11-AUG-2017  NAK   1.1      Modified theTargetObjectId to take contactId  
-- 25-SEP-2017  NAK   1.2      Dynamisme de l’envoie d’email     
-- 16-OCT-2017  NAK   1.3      Changement comportement adresses dynamiques
-- 14-AUG-2017  MGR   1.4   
-- 
------------------------------------------------------------------------------------------------*/    
    
    public static Boolean boolRunOnce = false;


    public static void sendEmail(List<zuora__Subscription__c> zuoraSubLst,Set<Id> zuoraAccIdSet){
        System.debug('## START of AP01_SendRSIEmail.sendEmail');

        boolRunOnce = true;

        Map<Id, String> accIdMailMap = new Map<Id, String>();
        Map<Id, Id> accIContactMap = new Map<Id, Id>();
        List<zuora__Subscription__c> updSubLst = new List<zuora__Subscription__c>();
        Map<Id,Zuora__SubscriptionProductCharge__c> productChargeSubIdMap = new Map<Id,Zuora__SubscriptionProductCharge__c>();
        //retrieving all org wide address params
        Map<String,SubscriptionEmailParam__c> OrgWideEmailAddressParams = SubscriptionEmailParam__c.getAll();

        String productName = System.label.productNameRenaultKwid;
        for (Zuora__SubscriptionProductCharge__c currentPro : [SELECT Zuora__Subscription__c
                                                               FROM Zuora__SubscriptionProductCharge__c
                                                               WHERE Zuora__Product__r.name = :productName
                                                               AND Zuora__Subscription__c IN :zuoraSubLst]){
            productChargeSubIdMap.put(currentPro.Zuora__Subscription__c,currentPro);
        }


        for(Account currentAcc : [SELECT Id
                                        ,personemail
                                        ,CompanyName__c
                                  FROM Account 
                                  WHERE Id IN :zuoraAccIdSet]){
            accIdMailMap.put(currentAcc.Id,currentAcc.personemail);
        }

        //MGR 11-08-2017 : <<START>>
        for (Contact current : [select accountId, 
                                       name 
                                FROM contact 
                                WHERE accountId IN: zuoraAccIdSet]){

            accIContactMap.put(current.accountId, current.Id);
        }
        //MGR 11-08-2017 : <<END>>

        System.debug('## accIdMailMap ' + accIdMailMap);
        for(Zuora__Subscription__c currentSubs : zuoraSubLst){
             //NAK 16-10-2017 : Changement comportement adresses dynamiques 
            //String orgWideAddName = OrgWideEmailAddressParams.get(currentSubs.WebsiteSource__c)!=null ? OrgWideEmailAddressParams.get(currentSubs.WebsiteSource__c).RecipientOrgWideName__c : OrgWideEmailAddressParams.get(AP_Constant.OrgWideDefault).RecipientOrgWideName__c;
            String orgWideAddName = OrgWideEmailAddressParams.get(currentSubs.WebsiteSource__c)!=null ? OrgWideEmailAddressParams.get(currentSubs.WebsiteSource__c).Organization_Wide_Address_Id__c : OrgWideEmailAddressParams.get(AP_Constant.OrgWideDefault).Organization_Wide_Address_Id__c;
            
            if(productChargeSubIdMap.containskey(currentSubs.Id)){
                if (accIdMailMap.containskey(currentSubs.Zuora__Account__c)){
                    createEmail(accIdMailMap.get(currentSubs.Zuora__Account__c),currentSubs.Id, accIContactMap.get(currentSubs.Zuora__Account__c),orgWideAddName);
                    updSubLst.add(new Zuora__Subscription__c(id = currentSubs.Id
                                                             ,Tech_EmailNotification__c = true));
                }
            }
        }

        System.debug('updSubLst ' + updSubLst);
        if (updSubLst.size() > 0){
            update updSubLst;
        }

       System.debug('## END of AP01_SendRSIEmail.sendEmail'); 
    }

    
    @future(callout=true)
    public static void createEmail(String emailToAdd, Id whatId, Id theTargetObjectId,String orgWideEmailAddressName){
        
        System.debug('## START of AP01_SendRSIEmail.createEmail');

        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> ();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        List <String> emailRecipients = new List<String>();
        
        EmailTemplate emailTemplate = [SELECT id
                                             ,DeveloperName
                                             ,body
                                             ,HtmlValue
                                             ,Subject 
                                     FROM EmailTemplate 
                                    WHERE DeveloperName = 'BSP_EmailKwid'];

        //NAK 16-10-2017 : Changement comportement adresses dynamiques <<START>>                        
        // MGR 14-08-2017 : <<START>>
        /*list <OrgWideEmailAddress> orgWideIdList  = [SELECT Id
                                                    FROM OrgWideEmailAddress 
                                                    //WHERE DisplayName =: system.label.AP01OrgWideAddDisplayName 
                                                    WHERE DisplayName =: orgWideEmailAddressName 
                                                    LIMIT 1];   
        if (orgWideIdList.size()>0){
            mail.setOrgWideEmailAddressId(orgWideIdList[0].Id);
        }*/
        //MGR 14-08-2017 : <<END>>
         mail.setOrgWideEmailAddressId((Id)orgWideEmailAddressName);
        //NAK 16-10-2017 : Changement comportement adresses dynamiques <<END>> 

        emailRecipients.add(emailToAdd);
        mail.setToAddresses(emailRecipients);   
        mail.setTemplateID(emailTemplate.id);

        //mail.setSubject('contrato de extensão da garantia');

        mail.setBccSender(false);
        mail.setUseSignature(false);    
        mail.setTargetObjectId(theTargetObjectId);
        mail.setWhatId(whatId);
        mail.saveAsActivity = false;
        List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();

        PageReference pageref = Page.VFP01_RenaultKwid;
        pageref.getParameters().put('Id',whatId);
        pageref.setRedirect(true);
        Blob b = Test.isRunningTest() == false ? pageref.getContent() : blob.valueof('dummyValue');   

        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        
        //set dynamic attachment name
        String dynamicDocName = System.Label.EmailDynamicDocRenaultKwid;
        efa.setFileName(dynamicDocName);         
        efa.setBody(b); 
        fileAttachments.add(efa);

        //Create Static Attachment
        StaticResource attachment1 = [SELECT body
                                             ,name 
                                      FROM StaticResource 
                                      WHERE name = 'RenalutKwidEmailStatic'];

        System.debug('### attachment1: '+attachment1); 
           
        Messaging.EmailFileAttachment objPDFAttachment = new Messaging.EmailFileAttachment();  
        String staticDocName = System.Label.EmailDocRenaultKwid;
        Blob staticDocBlob = attachment1.Body;
        objPDFAttachment.setFileName(staticDocName);
        objPDFAttachment.setBody(staticDocBlob);     
            
        fileAttachments.add(objPDFAttachment);
        mail.setFileAttachments(fileAttachments);
        messages.add(mail);        
       
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages); 
        system.debug('##### results: '+results);          
        if (results[0].success) {
            System.debug('The email was sent successfully.'); 
            system.debug('##### messages: '+messages);                
        } else {
            System.debug('The email failed to send: '+ results[0].errors[0].message);            
        }        
        
        //Insert EmailMessage
        string emailbodyPlainText = emailTemplate.HtmlValue != null ? (emailTemplate.HtmlValue).stripHtmlTags() : ' ';           

        Zuora__Subscription__c sub = [SELECT Zuora__Account__c
                                            ,Zuora__Account__r.personEmail 
                                      FROM Zuora__Subscription__c 
                                      WHERE id =: whatId ];
        System.debug('## sub ' + sub);

        //NAK 16-10-2017 : Update for FromName + From Add
        System.debug('## orgWideAdd ' + orgWideEmailAddressName);
        OrgWideEmailAddress orgWideAdd = [SELECT DisplayName
                                                ,Id
                                                ,Address
                                        FROM OrgWideEmailAddress 
                                        WHERE Id =: orgWideEmailAddressName];  
             

        EmailMessage emailMessage = new EmailMessage(FromName = orgWideAdd.DisplayName
                                                    ,FromAddress = orgWideAdd.Address
                                                    ,ToAddress = sub.Zuora__Account__r.personEmail
                                                    ,Subject = emailTemplate.Subject
                                                    ,TextBody = emailbodyPlainText
                                                    ,RelatedToId = whatId
                                                    ,MessageDate = system.Today()
                                                    ,Status = '3'
                                                    ,Account__c = sub.Zuora__Account__c );   
        insert emailMessage;

        list <Attachment> attachmentList = new list <Attachment>{
            new Attachment(ParentId = emailMessage.Id
                          ,Name = dynamicDocName
                          ,ContentType = 'application/pdf'
                          ,Body = b)
                                                               

          ,new Attachment(ParentId = emailMessage.Id 
                         ,Name = staticDocName
                         ,ContentType = 'application/pdf'
                         ,Body = staticDocBlob)
        };
        insert attachmentList;
    }


}