@isTest
public with sharing class VFC01_Voucher_TEST {
/* 
----------------------------------------------------------------------
-- - Name          : VFC01_Voucher_TEST
-- - Description   : Test class for VFC01_Voucher      
-- Maintenance History:
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  ---------------------------------------
-- 14-Jun-2017  DMG    1.0      Initial version 
---------------------------------------------------------------------
*/


    //variables
    static User testUser;
    static Zuora__Subscription__c zuora1;
    static list<Account> personList;
    static Zuora__CustomerAccount__c customerAccount1;
    static Zuora__SubscriptionProductCharge__c productCharge1;
    static VFC01_Voucher flowController;
    static PageReference pagevf;

    //static 
    public static String getUserNamePrefix(){
        return UserInfo.getOrganizationId() + System.now().millisecond();       
    }
    
    static{
        
        Id personRecordTypeId = [Select Id From RecordType  
                Where name = 'Person Account - Individual'].Id;

        Id profileId = [Select Id From Profile 
                Where name = 'Administrateur système' 
                    OR name = 'System Administrator'
                    OR name = 'Amministratore del sistema'
                    OR name = 'Systemadministrator'
                    OR name = 'Systemadministratör'
                    OR name = 'Administrador do sistema'
                    OR name = 'Systeembeheerder'
                    OR name = 'Systemadministrator'].Id;
        
        testUser = new User(
                        alias = 'testUsr1'
                        ,email='testUser1@noemail.com'
                        ,emailencodingkey='UTF-8'
                        ,lastname='testUser1'
                        ,LanguageLocaleKey = 'en_US'
                        ,LocaleSidKey = 'en_US'
                        ,profileid = profileId
                        ,timezonesidkey='Europe/Paris'
                        ,username=getUserNamePrefix() + 'standarduser1@testorg.com'                     
                    );
        
        system.runAs(testUser){

        personList = new list<Account>{
                new Account(AccountCountry__c='DE',
                            AccountSource='Website',
                            BillingCity='Stuttgart',
                            BillingCountry='Germany',
                            BillingPostalCode='70173',
                            BillingStreet='175 Königstrasse',
                            Citizenship__pc='Germany',
                            ContactFunctionalID__pc='Pesel Regon',
                            EligibleToCrmActions__c=false,
                            FirstName='Yann',
                            Gender__pc='Male',
                            LastName='Martin',
                            PersonBirthdate=Date.today(),
                            PersonEmail='zdouiri@test.com', 
                            PersonMobilePhone='+44 20 7489 79 99',
                            Phone='07700 900709',
                            RecordTypeId=personRecordTypeId,
                            Salutation='Mr.',
                            Type='Prospect'),
                new Account(AccountCountry__c='UK',
                            AccountSource='Website',
                            Citizenship__pc='United Kingdom',
                            ContactFunctionalID__pc='Pesel Regon Test',
                            EligibleToCrmActions__c=false,  
                            FirstName='Giorgio',
                            Gender__pc='Male',
                            LastName='Torres',
                            PersonBirthdate=Date.today(),
                            PersonEmail='giorgio.torres@test.com',
                            PersonMobilePhone='+44 20 7489 79 99',
                            Phone='07700 900709',   
                            RecordTypeId=personRecordTypeId,
                            Salutation='Mr.',
                            Type='Client')
        };
        insert personList;

        customerAccount1= new Zuora__CustomerAccount__c(Name='Yann Martin',
                                                    Zuora__AccountNumber__c='A00000016',
                                                    Zuora__Account__c=personList[0].Id,     
                                                    Zuora__AutoPay__c=true, 
                                                    Zuora__Balance__c=60.17,    
                                                    Zuora__Batch__c='Batch1',   
                                                    Zuora__BcdSettingOption__c='ManualSet', 
                                                    Zuora__BillCycleDay__c='12th of the month', 
                                                    Zuora__BillToAddress1__c='175 Königstrasse',    
                                                    Zuora__BillToCity__c='Stuttgart',   
                                                    Zuora__BillToCountry__c ='Germany', 
                                                    Zuora__BillToName__c='Yann Martin', 
                                                    Zuora__BillToPostalCode__c='70173',     
                                                    Zuora__BillToWorkEmail__c=' megbewole@csc.com', 
                                                    Zuora__CreditBalance__c =0.0,
                                                    Zuora__Credit_Balance__c=0.0,
                                                    Zuora__LastInvoiceDate__c=Date.today()+360, 
                                                    Zuora__MRR__c=30.98,        
                                                    Zuora__PaymentTerm__c='Due Upon Receipt',   
                                                    Zuora__SoldToAddress1__c='  175 Königstrasse',  
                                                    Zuora__SoldToCity__c='Stuttgart',   
                                                    Zuora__SoldToCountry__c ='Germany',
                                                    Zuora__SoldToPostalCode__c='70173', 
                                                    Zuora__SoldToWorkEmail__c='megbewole@csc.com',  
                                                    Zuora__Status__c='Active',  
                                                    Zuora__TaxExemptStatus__c='No', 
                                                    Zuora__TotalInvoiceBalance__c=60.17);

        insert customerAccount1;

        zuora1= new Zuora__Subscription__c( InsuredAccount__c=personList[1].Id,
                                            Name='A-S00000024',
                                            Policy_Number__c='AFF/RCI/0001 - OPN_numer systemowy RCI',
                                            Zuora__Account__c=personList[0].Id,
                                            Zuora__ContractEffectiveDate__c=Date.today()+60,
                                            Zuora__InitialTerm__c='12 Months',
                                            Zuora__MRR__c=11.99,
                                            Zuora__NextRenewalDate__c=  Date.today()+160,   
                                            Zuora__OriginalCreated_Date__c=Date.today()+60,
                                            Zuora__RenewalTermPeriodType__c='Month',
                                            Zuora__RenewalTerm__c='6 Months',
                                            Zuora__ServiceActivationDate__c=Date.today()+60,    
                                            Zuora__Status__c='Active',
                                            Zuora__SubscriptionEndDate__c=Date.today()+160,
                                            Zuora__SubscriptionNumber__c='A-S00000024',
                                            Zuora__SubscriptionStartDate__c=Date.today()+60,    
                                            Zuora__TCV__c=143.11,
                                            Zuora__TermEndDate__c=Date.today()+160,
                                            Zuora__TermSettingType__c='TERMED', 
                                            Zuora__TermStartDate__c=Date.today()+160,   
                                            Zuora__Version__c=3.0,
                                            Zuora__CustomerAccount__c=customerAccount1.Id,
                                            Zuora__InvoiceOwner__c=customerAccount1.Id,
                                            VoucherNumber_PL__c = 'test',
                                            Zuora__Zuora_Id__c='2c92c0f95c65768c015c81f5dbb70bcf');
        insert zuora1;

        productCharge1= new Zuora__SubscriptionProductCharge__c(Name='Financial Cover up to 1000€',
                                                                Zuora__Account__c=personList[0].Id,
                                                                Zuora__Subscription__c=zuora1.Id,
                                                                Zuora__Price__c=50.00
                                                                );

        insert productCharge1;
        }

    }
    @isTest
    static void testCheckInfo() {
        system.runAs(testUser){
            test.startTest();
                pagevf = Page.VFP01_Voucher;
                Test.setCurrentPage(pagevf);            
                ApexPages.currentPage().getParameters().put('Id',zuora1.Id); 
                flowController = new VFC01_Voucher();
 
            test.stopTest();
            List<Zuora__Subscription__c> lstSubscription=[select id from Zuora__Subscription__c where Id=:zuora1.Id];
            System.assertEquals(1, lstSubscription.size());
        }
    }
    @isTest
    static void testError() {
        system.runAs(testUser){
            test.startTest();
                pagevf = Page.VFP01_Voucher;
                Test.setCurrentPage(pagevf);            
                flowController = new VFC01_Voucher();
 
            test.stopTest();
            List<Zuora__Subscription__c> lstSubscription=[select id from Zuora__Subscription__c where Id=:zuora1.Id];
            System.assertEquals(1, lstSubscription.size());
        }
    }
}