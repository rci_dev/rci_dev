/** 
* @author: Adolphe SONKO
* @date : Création 24/07/2017
* @date : Modification 24/07/2017
* @description : this Class allow to manage Account business entity data preparation .
*/
public class EM001_Account {
    
	private static String CLASS_NAME = 'EM001_Account';
    
    /**
     * @author : Adolphe SONKO
     * @date : Création 24/07/2017
     * @date : Modification 24/07/2017 
     * @description : This method performs an SQL query on the account to repatriate its data from an ID list.
     * @param : Set<Id> AccountIds This parameter is a list of unique identifiers of account.
     * @return : List<Account> The returned item is a list of Accounts.
     */   
    public static List<Account> getListByIds(Set<Id> AccountIds){
        
        String methodName = 'getListByIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[AccountIds] :' + AccountIds);
        
        List<Account> listAccounts = DM001_Account.getListByIds(AccountIds);
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listAccounts);
        return listAccounts; 
    }
    
    
    /**
     * @author : Adolphe SONKO
     * @date : Création 24/07/2017
   	 * @date : Modification 24/07/2017 
     * @description : This method performs an SQL query on the Billing Entity to repatriate its data from an Account Country.
     * @param : Set<String> setAccountCountries This parameter is a list of Account countries.
     * @return : List<zqu__BillingEntity__c> The returned item is a list of Billing Entity.
     */
    public static List<zqu__BillingEntity__c> getListBillingEntitiesByCountries(Set<String> setAccountCountries){
        
        String methodName = 'getListBillingEntitiesByCountry';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[setAccountCountries] :' + setAccountCountries);
        
        List<zqu__BillingEntity__c> listBillingEntities = DM001_Account.getListBillingEntitiesByCountries(setAccountCountries);
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listBillingEntities);
        return listBillingEntities; 
    }
    
    
    public static zqu__BillingEntity__c getListBillingEntitiesByCountry(String country){
        
        String methodName = 'getListBillingEntitiesByCountry';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[country] :' + country);
        
        zqu__BillingEntity__c billingEntity = DM001_Account.getListBillingEntitiesByCountry(country);
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + billingEntity);
        return billingEntity; 
    }
    
}