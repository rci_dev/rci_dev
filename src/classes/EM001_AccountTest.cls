@isTest
private class EM001_AccountTest
{

	static Set<Id> AccountIds =  new Set<Id>();
	static Set<String> SetAccountCountries =  new Set<String>();
	static List<Account> accs;
	static List<zqu__BillingEntity__c> billingEntities;
	static String testCountry;
	static User testUser = TestFactory.getStandardUser();

	@isTest
	static void TestGetListByIds()
	{
		system.runAs(testUser){
			//data preparation
			accs = new List<Account>{
				new Account(
						Name = 'test_acc1'
					),
				new Account(
						Name='test_acc2'
					)
			};

			Test.startTest();
			insert accs;
			Test.stopTest();

			AccountIds.add(accs[0].id);
			AccountIds.add(accs[1].id);


			//asserts
			system.assertEquals(2 , EM001_Account.getListByIds(AccountIds).size());
		}

	}

	@isTest
	static void TestGetListBillingEntitiesByCountries(){

		system.runAs(testUser){

			//data preparation
			SetAccountCountries.add('DE');
			SetAccountCountries.add('MU');

			billingEntities = new List<zqu__BillingEntity__c>{
				new zqu__BillingEntity__c(Name = 'Test_billing_entities_1',
											zqu__EntityName__c = 'DE',
											zqu__EntityID__c = 'DE'),
				new zqu__BillingEntity__c(Name = 'Test_billing_entities_1',
											zqu__EntityName__c = 'MU',
											zqu__EntityID__c = 'MU')				
			};

			Test.startTest();
			insert billingEntities;
			Test.stopTest();

			System.assertEquals(2, EM001_Account.getListBillingEntitiesByCountries(SetAccountCountries).size());

		}
	}

	@isTest
	static void TestGetListBillingEntitiesByCountry(){
		System.runAs(testUser){
			//data preparation
			testCountry = 'GERMANY';
			zqu__BillingEntity__c billingEntityTest = new zqu__BillingEntity__c(Name = 'Test_billing_entities_1',
											zqu__EntityName__c = 'MU',
											zqu__EntityID__c = 'MU');
			Test.startTest();
			insert billingEntityTest;
			Test.stopTest();
			
			System.assertEquals(null, EM001_Account.getListBillingEntitiesByCountry(testCountry));
			System.assertEquals('MU' , EM001_Account.getListBillingEntitiesByCountry('MU').zqu__EntityName__c);

		}
	}

}