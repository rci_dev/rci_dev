public with sharing class VFC01_CyberRiskPolices {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_CyberRiskPolices
-- - Description   : fetch all details on the object Zuora__Subscription__c
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 12-JUN-2017  DMG   1.0      Initial version
------------------------------------------------------------------------------------------------
*/
    public String currentSubsId {get;set;}
    public Zuora__Subscription__c currentSub{get;set;}
    public Zuora__SubscriptionProductCharge__c currentSubProd{get;set;}
    
    public VFC01_CyberRiskPolices() {
        currentSubsId = ApexPages.currentPage().getParameters().get('Id');
        try{
            currentSub = [SELECT   id
                        ,InvoiceNumber__c 
                        ,Policy_Number__c
                        ,InsuredAccount__r.contactfunctionalid__pc
                        ,InsuredAccount__r.firstname
                        ,InsuredAccount__r.lastname
                        ,Zuora__Account__r.billingaddress
                        ,Zuora__Account__r.billingcity
                        ,Zuora__Account__r.billingpostalcode
                        ,Zuora__Account__r.billingstreet
                        ,Zuora__Account__r.contactfunctionalid__pc
                        ,Zuora__Account__r.firstname
                        ,Zuora__Account__r.lastname
                        ,Zuora__Account__r.BillingAddressLine1__c
                        ,Zuora__Account__r.BillingAddressLine2__c
                        ,Zuora__Account__r.BillingAddressLine3__c
                        ,Zuora__OriginalCreated_Date__c
                        ,LastCoveredDay__c
                        ,Zuora__TermStartDate__c 
                        ,Zuora__Account__r.BillingCity__c 
                        ,Zuora__Account__r.BillingPostalCode__c 
               FROM Zuora__Subscription__c
               WHERE id = :currentSubsId];
               currentSubProd = [select Id
                            ,Zuora__Price__c
                            ,zuora__productname_product__c
                            FROM Zuora__SubscriptionProductCharge__c
                            where Zuora__Subscription__c= : currentSubsId 
                            limit 1];
        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
           }

        

    }
}