/** 
* @author: Adolphe Sonko
* @date : Création 10/07/2017
* @date : Modification 10/07/2017
* @description : Build complete objects for unit testing development / public constants for unit tests.
*/
public class TestFactory {

    /* -------------------------------- *
     * Records creation methods 
     * ---------------------------------*/
    
    public static String getUserNamePrefix(){
        return UserInfo.getOrganizationId() + System.now().millisecond();       
    }
    
    public static Profile getSimpleProfile(){
        Profile prof = [SELECT Id FROM Profile Where name = 'Administrateur système' 
                                                OR name = 'System Administrator'
                                                OR name = 'Amministratore del sistema'
                                                OR name = 'Systemadministrator'
                                                OR name = 'Systemadministratör'
                                                OR name = 'Administrador do sistema'
                                                OR name = 'Systeembeheerder'
                                                OR name = 'Systemadministrator' LIMIT 1]; 
        return prof;
    }
    
    
    public static final Map <String, Schema.SObjectType> schemaMap;
     static {
        SchemaMap = Schema.getGlobalDescribe();
    }
    
    
    
    // User
    public static User getStandardUser()
    {
        Profile profile = getSimpleProfile();
        
        User u = new User
            (
                alias = 'testUsr1'
                ,email='testUser1@noemail.com'
                ,emailencodingkey='UTF-8'
                ,lastname='testUser1'
                ,LanguageLocaleKey = 'en_US'
                ,LocaleSidKey = 'en_US'
                ,profileid = profile.Id
                ,timezonesidkey='Europe/Paris'
                ,username=getUserNamePrefix() + 'standarduser1@testorg.com'
            );
        return u;
    }
    
    public static RecordType getSimpleRecordTypeIndividual(){
        RecordType personRecordType = [Select Id From RecordType Where name = 'Person Account - Individual'];
        return personRecordType;
    }
 /*   Id personRecordTypeId = [Select Id From RecordType  
                             Where name = 'Person Account - Individual'].Id; */
    
        Id profileId = [Select Id From Profile 
                        Where name = 'Administrateur système' 
                        OR name = 'System Administrator'
                        OR name = 'Amministratore del sistema'
                        OR name = 'Systemadministrator'
                        OR name = 'Systemadministratör'
                        OR name = 'Administrador do sistema'
                        OR name = 'Systeembeheerder'
                        OR name = 'Systemadministrator'].Id;
        
         
    // Account
    public static Account getSimpleAccount()
    {
        Account account = new Account 
            (
                Name = 'Company Test'   
            );
        return account;
    }
    
    
    // Subscription 
    public static Zuora__Subscription__c getSimpleZuoraSubscription()
    {
        Zuora__Subscription__c sub = new Zuora__Subscription__c
            (
                
                Name='Test Sub',
                Zuora__Zuora_Id__c = 'Test_Sub_XXXXXXXXXXXBSP01'
            );
        return sub;
        
        
    }
    
    
    //Zuora__SubscriptionProductCharge__c
    public static Zuora__SubscriptionProductCharge__c getSimpleZuoraSubscriptionProductCharge(String PrdName, String PrdSKU, Zuora__Subscription__c parentZSub)
    {
        
         Zuora__Product__c prdt1 = new Zuora__Product__c 
        (
        	Name = PrdName,
            Zuora__EffectiveEndDate__c = Date.newInstance(2047, 2, 17),
            Zuora__SKU__c = PrdSKU
            
        );
        
        insert prdt1;
        
        
        Zuora__SubscriptionProductCharge__c prdt = new Zuora__SubscriptionProductCharge__c
            (
                
            Zuora__Subscription__c = parentZSub.Id,
            Name='TEST',
            Zuora__Zuora_Id__c='TEST_BSP000001',
            Zuora__Product__c = prdt1.Id
            
            );
        
        return prdt;
       
        
    }
    
    
  
    
}