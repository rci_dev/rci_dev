@isTest
private class ObjectUtilTest
{
	static List<String> listZuoraIds;
	static Set<String> setFilterConditions;
	static String query;
	static List<Account> testAccounts;

	@isTest
	static void testObjectUtil()
	{

		listZuoraIds = new List<String>{
			'001' ,'002'
		};
		query =  'select SubscriptionNumber, InvoiceId From InvoiceItem where ' ;
		
		setFilterConditions = new Set<String>{
			'EW' , 'SW'
		};	
		testAccounts = new List<Account>{
			new Account(Name='testName'), 
			new Account(Name='testName2')
		};


		ObjectUtil.getFieldsListFor('Account');
		ObjectUtil.constructFieldListForQueryFrom('Account');
		ObjectUtil.debugLogBegin('classNameTest', 'methodNameTest', 'messageTest');
		ObjectUtil.debugLogEnd('classNameTest', 'methodNameTest', 'messageTest');
		ObjectUtil.debugLog('classNameTest', 'methodNameTest', 'messageTest');
		ObjectUtil.getPicklistValues('Account', 'BillingCountry__c');
		ObjectUtil.buildOrQueryFilter('InvoiceId', listZuoraIds, query, null, setFilterConditions);
		ObjectUtil.isEmpty(new Account());
		ObjectUtil.isEmpty(true);
		ObjectUtil.isEmpty('true');
		ObjectUtil.insertRecords(testAccounts);
		ObjectUtil.updateRecords(testAccounts);
		ObjectUtil.deleteRecords(testAccounts);
		ObjectUtil.getZuoraParametersByName('zuora_param');
	}	
}