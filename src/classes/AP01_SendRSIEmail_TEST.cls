@isTest(seeAllData=true)
private class AP01_SendRSIEmail_TEST {
/*
------------------------------------------------------------------------------------------------
-- - Name          : AP01_SendRSIEmail_TEST
-- - Description   : Test Class for AP01_SendRSIEmail
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 22-JUL-2017  NAK   1.0      Initial Version
-- 16-OCT-2017  NAK   1.1      Changement comportement adresses dynamiques  
------------------------------------------------------------------------------------------------*/   
    
    static User testUser;
    static list <Zuora__Subscription__c>  subscriptionList;
    static List<Account> accList = new list<Account>();
    static Account accSubNoEmail;
    static list <Zuora__SubscriptionProductCharge__c> prodChargeList;
    static list <Zuora__Product__c> productList;
    static Map<String,SubscriptionEmailParam__c> OrgWideEmailAddressParams = new Map<String,SubscriptionEmailParam__c>();
    
    static {
        system.debug('>>>>> START STATIC <<<<<');

        testUser = AP_Constant.createAdminUser('TestUserTriggerXYZ', AP_Constant.getProfileAdminId()); 
        testUser.BypassValidationRule__c = true;
        insert  testUser;

        System.runAs(testUser){

            //create account with mail 
            for (Integer i = 0;i < 10; i++){
                Account acc = AP_Constant.createAccount('Acc' + i);
                acc.CompanyName__c = 'testCompany';
                accList.add(acc);          
            }
            accList[0].PersonEmail = 'salesforce@sc-mauritius.com';
            accList[0].RecordTypeId = AP_Constant.MapRecordType('Account').get('PersonAccountIndividual'); //'0120Y000000Vf5K';
            insert accList;

            //create account without mail 
            accSubNoEmail = AP_Constant.createAccount('Test Account');
            accSubNoEmail.CompanyName__c = 'testCompany';
            accSubNoEmail.RecordTypeId = AP_Constant.MapRecordType('Account').get('PersonAccountIndividual');
            insert accSubNoEmail;   

            //select Folder 
            Id folderId = [SELECT id, DeveloperName, Name FROM Folder where DeveloperName = 'BSP_EmailTemplates'].Id;

            //create EmailTemplate
            list <EmailTemplate> emailTemplateList = new list <EmailTemplate> {
                new EmailTemplate(Name = 'EmailVoucher'
                                 ,DeveloperName = 'BSP_EmailMaintenance1'
                                 ,Markup = '<messaging:emailTemplate subject="Prezent na powitanie - Voucher assistance" recipientType="User" relatedToType="Zuora__Subscription__c"> <messaging:htmlEmailBody > <c:C01_EmailVoucher subsId="{!relatedTo.Id}"/> </messaging:htmlEmailBody> </messaging:emailTemplate>'
                                 ,IsActive = true
                                 ,FolderId = folderId
                                 ,TemplateType = 'visualforce')
            };
            insert emailTemplateList;   

            subscriptionList = new list<Zuora__Subscription__c>{
                new Zuora__Subscription__c(Name = 'Sub1'
                                          ,SubscriptionCountry__c = 'UK'
                                          ,CurrencyIsoCode = 'EUR'
                                          ,Zuora__Zuora_Id__c = 'id02'
                                          ,Zuora__Account__c = accList[0].Id
                                          ,Tech_SyncCommerceCloud__c = false
                                          ,Policy_Number__c = 'P01'
                                          ,Tech_EmailNotification__c = false
                                          ,TECH_COUNT_ProductCharges__c = 1  
                                          ,VoucherNumber_PL__c = 'test'
                                        //  ,TECH_COUNT_RatePlan__c = 1
                                          )

                ,new Zuora__Subscription__c(Name = 'Sub2'
                                          ,SubscriptionCountry__c = 'UK'
                                          ,CurrencyIsoCode = 'EUR'
                                          ,Zuora__Zuora_Id__c = 'id02'
                                          ,Zuora__Account__c = accList[1].Id
                                          ,Tech_SyncCommerceCloud__c = false
                                          ,Policy_Number__c = 'P01'
                                          ,VoucherNumber_PL__c = 'test'
                                          ,Tech_EmailNotification__c = false
                                          //,TECH_COUNT_ProductCharges__c = 1  
                                          //,TECH_COUNT_RatePlan__c = 1
                                          )

            };
            insert subscriptionList;

            productList = new list<Zuora__Product__c>{
                new Zuora__Product__c(Name = System.label.productNameRenaultKwid)
            };
            insert productList;

            prodChargeList = new list<Zuora__SubscriptionProductCharge__c>{
                new Zuora__SubscriptionProductCharge__c(Name = 'Product2'
                                                        ,Zuora__Zuora_Id__c = 'id02'
                                                        ,Zuora__Product__c = productList[0].Id
                                                        ,Zuora__ProductName__c = System.label.productNameRenaultKwid
                                                        ,Zuora__Subscription__c = subscriptionList[0].Id)

            };
            insert prodChargeList;

            SubscriptionEmailParam__c subscriptionEmailParamDefault = SubscriptionEmailParam__c.getValues('Default');

            List<OrgWideEmailAddress> lstOrgWideEmailAddress = [SELECT Id FROM OrgWideEmailAddress WHERE displayname = 'Loja RCI Brasil' limit 1];

            if(subscriptionEmailParamDefault == null){
                //create custom setting records
                SubscriptionEmailParam__c EmailAddress = new SubscriptionEmailParam__c( RecipientOrgWideName__c =  'Loja RCI Brasil',
                                                                                        Name = 'Default',
                                                                                        Organization_Wide_Address_Id__c = lstOrgWideEmailAddress[0].Id);
                insert EmailAddress;
            }
            else{
                //update existing default custom setting record due to see all data
                subscriptionEmailParamDefault.Organization_Wide_Address_Id__c = lstOrgWideEmailAddress[0].Id;
                update subscriptionEmailParamDefault;
            }
        }
    }

    
    static testMethod void testUpdateSubscriptionPL() {
        System.debug('>>>TEST METHOD testInsertSubscriptionPL<<<');        

        System.runAs(testUser){         
          
            Test.startTest();    
                subscriptionList[0].Tech_SyncCommerceCloud__c = true;
                update subscriptionList[0];
            Test.stopTest(); 

            Zuora__Subscription__c zSubPLCheck = [SELECT id
                                                        ,Tech_EmailNotification__c 
                                                  FROM Zuora__Subscription__c 
                                                  WHERE id =:subscriptionList[0].Id];

            System.assertEquals(true, zSubPLCheck.Tech_EmailNotification__c);   
        }
    }

    static testMethod void testSubscriptionNoEmail() {
        System.debug('>>> TEST testSubscriptionNoEmail<<<');   

         System.runAs(testUser){         
          
            Test.startTest();    
                subscriptionList[1].Tech_SyncCommerceCloud__c = true;
                try{
                    update subscriptionList[1];
                }catch(Exception e){
                    system.debug('### Error');
                }
            Test.stopTest(); 
        

            Zuora__Subscription__c zSubPLCheck = [SELECT id
                                                        ,Tech_EmailNotification__c 
                                                  FROM Zuora__Subscription__c 
                                                  WHERE id =:subscriptionList[0].Id];

            System.assertEquals(false, zSubPLCheck.Tech_EmailNotification__c);   
        }
    }

  
    
}