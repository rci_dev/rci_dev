/** 
* @author: Adolphe SONKO
* @date : Création 02/02/2017
* @date : Modification 02/02/2017
* @description : this Class allow to manage Subscription business entity data preparation .
*/
public class EM003_Subscription {
		private static String CLASS_NAME = 'EM003_Subscription';
    
    /**
     * @author : Adolphe SONKO
     * @date : Création 02/02/2017
     * @date : Modification 02/02/2017 
     * @description : This method performs an SQL query on the subscription to repatriate its data from an ID list.
     * @param : Set<Id> SubscriptionIds This parameter is a list of unique identifiers of subscription.
     * @return : List<Zuora__Subscription__c> The returned item is a list of Subscriptions.
     */   
    public static List<Zuora__Subscription__c> getListByIds(Set<Id> SubscriptionIds){
        
        String methodName = 'getListByIds';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[SubscriptionIds] :' + SubscriptionIds);
        
        List<Zuora__Subscription__c> listSubscriptions = DM003_Subscription.getListByIds(SubscriptionIds);
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listSubscriptions);
        return listSubscriptions; 
    }
    
	/**
     * @author : Adolphe SONKO
     * @date : Création 02/02/2017
	 * @date : Modification 02/02/2017
     * @description : This method performs an SQL query on the subscription to repatriate its data from an String list.
     * @param : Set<String> subscriptionNumbers This parameter is a list of unique identifiers of subscription.
     * @return : List<Zuora__Subscription__c> The returned item is a list of Subscriptions.
     */ 
    public static List<Zuora__Subscription__c> getListBySubscriptionNumber(Set<String> subscriptionNumbers){
        
        String methodName = 'getListBySubscriptionNumber';    
        ObjectUtil.debugLogBegin(CLASS_NAME, methodName, '[subscriptionNumbers] :' + subscriptionNumbers);
        
        List<Zuora__Subscription__c> listSubscriptions = DM003_Subscription.getListBySubscriptionNumber(subscriptionNumbers);
        
        ObjectUtil.debugLogEnd(CLASS_NAME, methodName, '[RESULTS] :' + listSubscriptions);
        return listSubscriptions; 
    }
    
}