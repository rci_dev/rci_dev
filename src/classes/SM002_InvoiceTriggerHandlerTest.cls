/** 
* @author: Adolphe Sonko
* @date : Création 03/08/2017
* @date : Modification 03/08/2017
* @description : This Class allows to handle trigger events in Account entity.
*/
@isTest(SeeAllData=true)
public class SM002_InvoiceTriggerHandlerTest {

    //variables
  static Zuora__Subscription__c zuora1;
  static Zuora__Subscription__c zuora2;
  static list<Account> personList;
  static Zuora__CustomerAccount__c customerAccount1;
  static Zuora__CustomerAccount__c customerAccount2;
  static Zuora__ZInvoice__c inv;  
  static List<ZuoraParameters__c> zparam = new List<ZuoraParameters__c>();
  static List<zqu__BillingEntity__c> lstTestBillingEnt = new List<zqu__BillingEntity__c>();


  static User testUser = TestFactory.getStandardUser();
  //insert testUser;
  
    static testMethod void test(){
    System.runAs(testUser){
    //system.runAs(testUser){
    Test.startTest();
    
    // Data Preparation
    ZuoraParameters__c zparameterToDel1 = ZuoraParameters__c.getValues('SBX US');
    ZuoraParameters__c zparameterToDel2 = ZuoraParameters__c.getValues('PROD EU');

    if(zparameterToDel1 != null){
      delete zparameterToDel1;
    }

    if(zparameterToDel2 != null){
      delete zparameterToDel2;
    }

    zparam.add(new ZuoraParameters__c(Name = 'SBX US', SFDC_OrgId__c = UserInfo.getOrganizationId(), URL__c='https://rest.apisandbox.zuora.com'));
    zparam.add(new ZuoraParameters__c(Name = 'PROD EU', SFDC_OrgId__c = UserInfo.getOrganizationId(), URL__c='https://rest.apisandbox.zuora.com'));
    insert zparam;

    Account[] Acc = [SELECT ID FROM Account WHERE LastName = 'Martin2' OR LastName = 'Torres'];
    try{
        delete Acc;
    }
    catch(Exception e){
      System.debug('## Account DML Exception : ' + e.getMessage());
    }

    Zuora__CustomerAccount__c[] zAcc = [SELECT ID FROM Zuora__CustomerAccount__c WHERE Zuora__AccountNumber__c = 'A00000016' OR Zuora__AccountNumber__c = 'A99999998'];
    try{
        delete zAcc;
    }
    catch(Exception e){
      System.debug('## Customer Account DML Exception : ' + e.getMessage());
    }

    Zuora__Subscription__c[] zSubs = [SELECT ID FROM Zuora__Subscription__c WHERE Name='A-S99999999' OR Name='A-S99999998'];
    try{
        delete zSubs;
    }catch(Exception e){
      System.debug('## Subscriptions DML Exception : ' + e.getMessage());
    } 

    Zuora__ZInvoice__c[] zInv = [SELECT ID FROM Zuora__ZInvoice__c WHERE Name = 'Test Invoice'];
    try{
      delete zInv;
    }catch(Exception e){
      System.debug('## Zinvoice DML Exception : ' + e.getMessage());
    }

    zqu__BillingEntity__c[] zBill = [SELECT ID FROM zqu__BillingEntity__c WHERE Name = 'testBilling'];
    try{
      delete zBill;
    }catch(Exception e){
      System.debug('## Billing DML Exception : ' + e.getMessage());
    }
  
    personList = new list<Account>{
                new Account(AccountCountry__c='UK',
                      AccountSource='Website',
                      BillingCity='Stuttgart',
                      BillingCountry='United kingdom',
                      BillingPostalCode='70173',
                      BillingStreet='175 Königstrasse',
                      Citizenship__pc='Germany',
                      ContactFunctionalID__pc='Pesel Regon 1',
                      EligibleToCrmActions__c=false,
                      FirstName='Yann',
                      Gender__pc='Male',
                      LastName='Martin2',
                      PersonBirthdate=Date.today(),
                      PersonEmail='zdouiri@test.com',  
                      PersonMobilePhone='+44 20 7489 79 99',
                      Phone='07700 900709',
                      RecordTypeId=TestFactory.getSimpleRecordTypeIndividual().Id,
                      Salutation='Mr.',
                      Type='Prospect',
                      Tech_ZuoraAccountNumber__c='A00000016', 
                      Tech_Contact_Functional_ID__c = 'testFunctionalId'),
                new Account(AccountCountry__c='UK',
                      AccountSource='Website',
                      Citizenship__pc='United Kingdom',
                      ContactFunctionalID__pc='Pesel Regon 2',
                      EligibleToCrmActions__c=false,  
                      FirstName='Giorgio',
                      Gender__pc='Male',
                      LastName='Torres',
                      PersonBirthdate=Date.today(),
                      PersonEmail='giorgio.torres@test.com',
                      PersonMobilePhone='+44 20 7489 79 99',
                      Phone='07700 900709',  
                      RecordTypeId=TestFactory.getSimpleRecordTypeIndividual().Id,
                      Salutation='Mr.',
                      Type='Client',
                      Tech_ZuoraAccountNumber__c='A00000015',
                      Tech_Contact_Functional_ID__c = 'testFunctionalId2')
            };
    insert personList;
        
    customerAccount1 = new Zuora__CustomerAccount__c(Name='Yann Martin',
                                  Zuora__AccountNumber__c='A00000016',
                                  Zuora__Account__c=personList[0].Id,    
                                  Zuora__AutoPay__c=true,  
                                  Zuora__Balance__c=60.17,  
                                  Zuora__Batch__c='Batch1',  
                                  Zuora__BcdSettingOption__c='ManualSet',  
                                  Zuora__BillCycleDay__c='12th of the month',  
                                  Zuora__BillToAddress1__c='175 Königstrasse',  
                                  Zuora__BillToCity__c='Stuttgart',  
                                  Zuora__BillToCountry__c  ='Germany',  
                                  Zuora__BillToName__c='Yann Martin',  
                                  Zuora__BillToPostalCode__c='70173',    
                                  Zuora__BillToWorkEmail__c='  megbewole@csc.com',  
                                  Zuora__CreditBalance__c  =0.0,
                                  Zuora__Credit_Balance__c=0.0,
                                  Zuora__LastInvoiceDate__c=Date.today()+360,  
                                  Zuora__MRR__c=30.98,    
                                  Zuora__PaymentTerm__c='Due Upon Receipt',  
                                  Zuora__SoldToAddress1__c='  175 Königstrasse',  
                                  Zuora__SoldToCity__c='Stuttgart',  
                                  Zuora__SoldToCountry__c  ='Germany',
                                  Zuora__SoldToPostalCode__c='70173',  
                                  Zuora__SoldToWorkEmail__c='megbewole@csc.com',  
                                  Zuora__Status__c='Active',  
                                  Zuora__TaxExemptStatus__c='No',  
                                  Zuora__TotalInvoiceBalance__c=60.17);
        
    insert customerAccount1; 
        
    zuora1 = new Zuora__Subscription__c( InsuredAccount__c=personList[1].Id,
                              Name='A-S99999999',
                              Policy_Number__c='AFF/RCI/0001 - OPN_numer systemowy RCI',
                              Zuora__Account__c=personList[0].Id,
                              Zuora__ContractEffectiveDate__c=Date.today()+60,
                              Zuora__InitialTerm__c='12 Months',
                              Zuora__MRR__c=11.99,
                              Zuora__NextRenewalDate__c=  Date.today()+160,  
                              Zuora__OriginalCreated_Date__c=Date.today()+60,
                              Zuora__RenewalTermPeriodType__c='Month',
                              Zuora__RenewalTerm__c='6 Months',
                              Zuora__ServiceActivationDate__c=Date.today()+60,  
                              Zuora__Status__c='Active',
                              Zuora__SubscriptionEndDate__c=Date.today()+160,
                              Zuora__SubscriptionNumber__c='A-S99999999',
                              Zuora__SubscriptionStartDate__c=Date.today()+60,  
                              Zuora__TCV__c=143.11,
                              Zuora__TermEndDate__c=Date.today()+160,
                              Zuora__TermSettingType__c='TERMED',  
                              Zuora__TermStartDate__c=Date.today()+160,  
                              Zuora__Version__c=3.0,
                              Zuora__CustomerAccount__c=customerAccount1.Id,
                              Zuora__InvoiceOwner__c=customerAccount1.Id,
                              Zuora__Zuora_Id__c='2c92c0f95c65768c015c81f5dbb70bcf'); 
    insert zuora1; 

    customerAccount2 = new Zuora__CustomerAccount__c(Name='Veronique Coulon',
                                  Zuora__AccountNumber__c='A99999998',
                                  Zuora__Account__c=personList[1].Id,    
                                  Zuora__AutoPay__c=true,  
                                  Zuora__Balance__c=50.23,  
                                  Zuora__Batch__c='Batch1',  
                                  Zuora__BcdSettingOption__c='ManualSet',  
                                  Zuora__BillCycleDay__c='11th of the month',  
                                  Zuora__BillToAddress1__c='179 Königstrasse',  
                                  Zuora__BillToCity__c='Stuttgart',  
                                  Zuora__BillToCountry__c  ='Germany',  
                                  Zuora__BillToName__c='Veronique Coulon',  
                                  Zuora__BillToPostalCode__c='70179',    
                                  Zuora__BillToWorkEmail__c='  megbewole2@csc.com',  
                                  Zuora__CreditBalance__c  =0.0,
                                  Zuora__Credit_Balance__c=0.0,
                                  Zuora__LastInvoiceDate__c=Date.today()+360,  
                                  Zuora__MRR__c=30.98,    
                                  Zuora__PaymentTerm__c='Due Upon Receipt',  
                                  Zuora__SoldToAddress1__c='  179 Königstrasse',  
                                  Zuora__SoldToCity__c='Stuttgart',  
                                  Zuora__SoldToCountry__c  ='Germany',
                                  Zuora__SoldToPostalCode__c='70179',  
                                  Zuora__SoldToWorkEmail__c='megbewole2@csc.com',  
                                  Zuora__Status__c='Active',  
                                  Zuora__TaxExemptStatus__c='No',  
                                  Zuora__TotalInvoiceBalance__c=50.23);        
    insert customerAccount2;
            
    zuora2 = new Zuora__Subscription__c( InsuredAccount__c=personList[0].Id,
                              Name='A-S99999998',
                              Policy_Number__c='AFF/RCI/0002 - OPN_numer systemowy RCI',
                              Zuora__Account__c=personList[1].Id,
                              Zuora__ContractEffectiveDate__c=Date.today()+60,
                              Zuora__InitialTerm__c='12 Months',
                              Zuora__MRR__c=10.99,
                              Zuora__NextRenewalDate__c=  Date.today()+160,  
                              Zuora__OriginalCreated_Date__c=Date.today()+60,
                              Zuora__RenewalTermPeriodType__c='Month',
                              Zuora__RenewalTerm__c='6 Months',
                              Zuora__ServiceActivationDate__c=Date.today()+60,  
                              Zuora__Status__c='Active',
                              Zuora__SubscriptionEndDate__c=Date.today()+160,
                              Zuora__SubscriptionNumber__c='A-S99999998',
                              Zuora__SubscriptionStartDate__c=Date.today()+60,  
                              Zuora__TCV__c=143.11,
                              Zuora__TermEndDate__c=Date.today()+160,
                              Zuora__TermSettingType__c='TERMED',  
                              Zuora__TermStartDate__c=Date.today()+160,  
                              Zuora__Version__c=3.0,
                              Zuora__CustomerAccount__c=customerAccount2.Id,
                              Zuora__InvoiceOwner__c=customerAccount2.Id,
                              Zuora__Zuora_Id__c='2c92c0f95c65768c015c81f5dbb70bcg'); 
    insert zuora2; 
    
    inv = new Zuora__ZInvoice__c(Name = 'Test Invoice',
                                         Zuora__Account__c = personList[0].Id,
                                         Zuora__Zuora_Id__c = 'A-S99999999');
    //insert
    insert inv;


    lstTestBillingEnt.add(new zqu__BillingEntity__c(CurrencyIsoCode = 'GBP' , zqu__Country__c='United kingdom', Name='testBilling', zqu__EntityID__c='testentityId', zqu__EntityName__c = 'UK'));
    insert lstTestBillingEnt;

    Test.stopTest();
    }

    }
}