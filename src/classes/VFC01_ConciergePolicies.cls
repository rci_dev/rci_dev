public class VFC01_ConciergePolicies {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_ConciergePolicies
-- - Description   : fetch all details on the object Zuora__Subscription__c
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 09-JUN-2017  WCH   1.0      Initial version
-- 18-OCT-2017  MGR   1.1      Added Optin Partner Phone, Email, SMS must be checked
------------------------------------------------------------------------------------------------
*/
    public String strToday {get;set;}
    public String currentSubsId {get;set;}
    public Zuora__Subscription__c currentSub{get;set;}
    public Zuora__SubscriptionProductCharge__c currentSubProd{get;set;}
    public String RATEPLANNAME_GOLD {get;set;}
    public String RATEPLANNAME_SILVER  {get;set;}


    public VFC01_ConciergePolicies() {
        currentSubsId = ApexPages.currentPage().getParameters().get('Id');
        try{

            RATEPLANNAME_GOLD = 'Gold';
            RATEPLANNAME_SILVER = 'Silver';

            currentSub = [SELECT   id
                            ,Brand__c
                            ,InsuredAccount__r.contactfunctionalid__pc
                            ,InsuredAccount__r.firstname
                            ,InsuredAccount__r.lastname
                            ,InsuredAccount__r.personemail
                            ,InsuredAccount__r.personmailingpostalcode
                            ,InsuredAccount__r.personmobilephone
                            ,Model__c
                            ,isOptinPhoneSMSEmail__c //MGR 18-10-2017
                            ,PlateNumber__c
                            ,Policy_Number__c
                            ,VehicleFirstRegistrationDate__c
                            ,VIN__c
                            ,YearOfManufacture__c
                            ,OptinPartnerEmail__c
                            ,OptinPartnerPhone__c
                            ,OptinPartnerSms__c
                            ,RatePlanName__c
                            ,InsuredAccount__r.billingaddress
                            ,InsuredAccount__r.billingcity
                            ,InsuredAccount__r.billingpostalcode
                            ,InsuredAccount__r.billingstreet
                            ,InsuredAccount__r.BillingAddressLine1__c 
                            ,InsuredAccount__r.BillingAddressLine2__c 
                            ,InsuredAccount__r.BillingAddressLine3__c 
                            ,InsuredAccount__r.MailingAddressLine1__c
                            ,InsuredAccount__r.MailingAddressLine2__c
                            ,InsuredAccount__r.MailingAddressLine3__c
                            ,InsuredAccount__r.MailingAddressLine4__c
                            ,InsuredAccount__r.MailingpostalCode__c
                            ,InsuredAccount__r.MailingCountry__c
                            ,InsuredAccount__r.MailingDistrict__c
                            ,InsuredAccount__r.MailingCity__c
                            ,Zuora__Account__r.MailingCity__c
                            ,Zuora__Account__r.MailingAddressLine1__c 
                            ,Zuora__Account__r.MailingAddressLine2__c 
                            ,Zuora__Account__r.MailingAddressLine3__c 
                            ,Zuora__Account__r.billingaddress
                            ,Zuora__Account__r.billingcity
                            ,Zuora__Account__r.billingpostalcode
                            ,Zuora__Account__r.billingstreet
                            ,Zuora__Account__r.contactfunctionalid__pc
                            ,Zuora__Account__r.firstname
                            ,Zuora__Account__r.lastname
                            ,Zuora__Account__r.personbirthdate
                            ,Zuora__Account__r.personemail
                            ,Zuora__Account__r.personmailingpostalcode
                            ,Zuora__Account__r.personmobilephone
                            ,Zuora__OriginalCreated_Date__c
                            ,Zuora__TermStartDate__c 
                            ,LastCoveredDay__c 
                            ,InsuredAccount__r.BillingAddressLine4__c 
                            ,InsuredAccount__r.BillingPostalCode__c 
                            ,InsuredAccount__r.BillingCountry__c 
                            ,InsuredAccount__r.BillingDistrict__c 
                            ,InsuredAccount__r.BillingCity__c 
                            ,Zuora__Account__r.MailingDistrict__c 
                            ,Zuora__Account__r.MailingCountry__c 
                            ,Zuora__Account__r.MailingPostalCode__c 
                            ,Zuora__Account__r.MailingAddressLine4__c 
                            ,Zuora__Account__r.BillingCity__c 
                            ,Zuora__Account__r.BillingDistrict__c 
                            ,Zuora__Account__r.BillingCountry__c 
                            ,Zuora__Account__r.BillingPostalCode__c 
                            ,Zuora__Account__r.BillingAddressLine4__c 
                            ,Zuora__Account__r.BillingAddressLine3__c 
                            ,Zuora__Account__r.BillingAddressLine2__c 
                            ,Zuora__Account__r.BillingAddressLine1__c

                   FROM Zuora__Subscription__c
                   WHERE id = :currentSubsId];

            strToday = Datetime.now().format('dd/MM/YY');
            currentSubProd = [select Id
                                ,Zuora__Price__c
                                ,zuora__productname_product__c
                                FROM Zuora__SubscriptionProductCharge__c
                                where Zuora__Subscription__c= : currentSubsId 
                                limit 1];
        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
        }
    }
}