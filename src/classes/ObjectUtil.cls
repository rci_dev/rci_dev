/** 
* @author: Adolphe Sonko
* @date : Création 13/06/2017
* @date : Modification 13/06/2017
* @description : This class contains all the transverse utility methods .
*/
public class ObjectUtil {
    
    private static String CLASS_NAME = 'ObjectUtil';   
    public static final Map <String, Schema.SObjectType> schemaMap;
    
    public class Pair {
        public String key {get; set;}
        public String val {get; set;}
    }

    static {
        SchemaMap = Schema.getGlobalDescribe();
    }
    
    /**
   * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method allows to return a list of fields for a given object.
     * @param : String sObjectName This parameter is the name of sObject.
     * @return : List<String> The returned item is a list of the fields sObject parameter.
     */
    public static List<String> getFieldsListFor(String sObjectName) {
        System.debug('## sObject : ' + sObjectName);
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectName).getDescribe().fields.getMap();
        List<String> fieldsList = new List<String>();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            Schema.describefieldresult dfield = sfield.getDescribe();
            Pair field = new Pair();
            field.key = dfield.getname();
            fieldsList.add(field.key);
        }
        System.debug('## fieldsList : ' + fieldsList);
        return fieldsList;        
    }
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method allows to return a list of fields for a given object.
     * @param : String sObjectName This parameter is the name of sObject.
     * @return : String The returned item is a string of sObject the parameter fields.
     */
    public static String constructFieldListForQueryFrom(String sObjectName) {
        return constructFilteredFieldListForQueryFrom(sObjectName, '');
    }
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method used to return all fields EXCEPT those ending with a specified extension.
     * @param : String sObjectName This parameter is the name of sObject.
     * @param : String notEndingFilter Ce paramètre spécifie l'extension du nom api des champs à omettre.
     * @return : String The returned item is a string of sObject the parameter fields.
     */
    public static String constructFilteredFieldListForQueryFrom(String sObjectName, String notEndingFilter) {
        List<String> fieldsList = getFieldsListFor(sObjectName);
        String fieldsQuery = ' ';
        System.debug('## fieldsList : ' + fieldsList);
        for (String field : fieldsList) {
            if (String.isEmpty(notEndingFilter) || !field.endsWith(notEndingFilter) ){
                fieldsQuery = fieldsQuery + '' + field + ',';
            }
        }

        return fieldsQuery.replace(fieldsQuery, fieldsQuery.substring(0, fieldsQuery.length() - 1) + ' ');
    }    
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method provides the debug log some information in early method.
     * @param : String className This parameter is the name of the Apex class.
     * @param : String methodName This parameter is the name of the Apex method.
     * @param : String message This parameter represents the information to appear in the debug log.
     * @return : N/A
     */
     public static void debugLogBegin(String className, String methodName, String message){
        system.debug('### >> BEGIN ['+className+'] ['+methodName+'] [MESSAGE] : ' + message);
     }  
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016 
     * @description : This method provides the debug log on certain information to the method.
     * @param : String className This parameter is the name of the Apex class.
     * @param : String methodName This parameter is the name of the Apex method.
     * @param : String message This parameter represents the information to appear in the debug log.
     * @return : N/A
     */
     public static void debugLogEnd(String className, String methodName, String message){
        system.debug('### >> END ['+className+'] ['+methodName+'] [MESSAGE] : ' + message);
     }
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method provides the debug log some information in method.
     * @param : String className This parameter is the name of the Apex class.
     * @param : String methodName This parameter is the name of the Apex method.
     * @param : String message This parameter represents the information to appear in the debug log.
     * @return : N/A
     */
     public static void debugLog(String className, String methodName, String message){
        system.debug('### >> LOG ['+className+'] ['+methodName+'] [MESSAGE] : ' + message);
     }
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016 
     * @description : his method provides picklist values.
     * @param : String sObjectAPI This parameter is the API name of the object.
     * @param : String Field This parameter is the name of the picklist field of the object.
     * @return : List<String> The returned element is a character string list containing all the values of picklist.
     */     
     public static List<String> getPicklistValues(String sObjectAPI, String Field){ 
        
             List<String> lstPickvals=new List<String>();
             Schema.sObjectType sobject_type = Schema.getGlobalDescribe().get(sObjectAPI).newSObject().getSObjectType(); 
             Map<String, Schema.SObjectField> field_map = sobject_type.getDescribe().fields.getMap();
             List<Schema.PicklistEntry> pick_list_values = field_map.get(Field).getDescribe().getPickListValues(); 
             for (Schema.PicklistEntry a : pick_list_values) { 
                  lstPickvals.add(a.getValue());
             }
             return lstPickvals;
     }      
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method provides the id of the record type from the name of the record type.
     * @param : String objectName This parameter is the API name of the object.
     * @param : String recordtypeName This parameter is the name of the recording of the object type.
     * @return : Id The returned element is the identifier of the record type.
     */
    public static Id getRecordTypeId(String objectName, String recordtypeName){
        return schemaMap.get(objectName).getDescribe().getRecordTypeInfosByName().get(recordtypeName).getRecordTypeId();
    }
    
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method checks if a string value is empty.
     * @param : sObject value This parameter is the value to evaluate.
     * @return : Boolean True if the value is empty or null
     */
     public static Boolean isEmpty(sObject value){
        return (value == null);
     }
    
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016 
     * @description : This method checks if a string value is empty.
     * @param : Boolean value This parameter is the value to evaluate.
     * @return : Boolean True if the value is empty or null
     */
     public static Boolean isEmpty(Boolean value){
        return (value == null);
     }
    
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016 
     * @description : This method checks if a string value is empty.
     * @param : String stringValue This parameter is the name of the Apex class.
     * @return : Boolean True if the string is empty or null
     */
     public static Boolean isEmpty(String stringValue){
        return (stringValue == null || stringValue == '');
     }
    
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016 
     * @description : This method insert records in Database.
     * @param : List<SObject> records This parameter is the records to insert.
     * @return : N/A
     */
     public static void insertRecords(List<SObject> records){
        insert records;
     }
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016 
     * @description : This method update existing records in Database.
     * @param : List<SObject> records This parameter is the records to update.
     * @return : N/A
     */
     public static void updateRecords(List<SObject> records){
        update records;
     }
    
    
    /**
     * @author: Stéphane Trotto
   * @date : Création 27/10/2016
   * @date : Modification 27/10/2016
     * @description : This method delete existing records in Database.
     * @param : List<SObject> records This parameter is the records to delete.
     * @return : N/A
     */
     public static void deleteRecords(List<SObject> records){
        delete records;
     }
    
    
    /**
     * @auteur : Adolphe SONKO
     * @date : Creation 02/08/2017
     * @date : Modification 02/08/2017 
     * @description : This method return the Zuora Parameters By Name.
     * @param : String paramName This parameter is the Name of the Custom setting.
     * @return : ZuoraParameters__c
     */
     public static ZuoraParameters__c getZuoraParametersByName(String paramName){
     	return ZuoraParameters__c.getAll().get(paramName);
     }
    
    
    /**
     * @auteur : Adolphe SONKO
     * @date : Creation 02/08/2017
     * @date : Modification 02/08/2017 
     * @description : This method return the query string by filter .
     * @param : String fieldToFilter .
     * @return : String
     */
    public static String buildOrQueryFilter(String fieldToFilter, List<String> filterCriterias, String zuoraQuery, String initialCondition, Set<String> setFilterConditions) {
        String methodName = 'buildOrQueryFilter';
        
        if (String.isBlank(fieldToFilter) && String.isBlank(initialCondition)) {
            return null;
        }
        
        if (String.isBlank(fieldToFilter)) {
            zuoraQuery += initialCondition;
        } else {
            Integer counter = 0;
            Integer nbRecords = filterCriterias.size();
            
            if (String.isNotEmpty(initialCondition)) {
                initialCondition += ' AND ';
            } else {
                initialCondition = '';
            }
            
            for (String filterCriteria : filterCriterias) {
                
                counter++;
                
                String operator = '=';
                String startWith = '';
                String endWith = '';
                
                if (setFilterConditions != null) {
                    if (setFilterConditions.contains('SW')) {
                        operator = 'LIKE';
                        startWith = '%';
                    }
                    if (setFilterConditions.contains('EW')) {
                        operator = 'LIKE';
                        endWith = '%';
                    }
                }
                
                zuoraQuery += initialCondition + fieldToFilter + ' ' + operator + ' \'' + endWith + filterCriteria + startWith + '\'';
                
                if (counter < nbRecords) {
                    zuoraQuery += ' OR ';
                }
            }
        }
        
        return zuoraQuery;
    }
    
   
                       
}