@isTest
private class DM003_SubscriptionTest
{
	static List<Zuora__Subscription__c> LstSubscriptionsTest;
	static Set<ID> LstSubscriptionTestIds =  new Set<ID>();
	static Set<String> LstSubsName = new Set<String>();
	@isTest
	static void DM003_SubscriptionTest()
	{	
		//prepare data for testing getListByIds() and getListBySubscriptionNumber() method
		LstSubscriptionsTest = new List<Zuora__Subscription__c>{
			new Zuora__Subscription__c(Name = 'Test_sub_1'),
			new Zuora__Subscription__c(Name = 'Test_sub_2')
		};

		insert LstSubscriptionsTest;

		for(Zuora__Subscription__c subs : LstSubscriptionsTest){
			LstSubscriptionTestIds.add(subs.ID);
			LstSubsName.add(subs.Name);
		}

		DM003_Subscription.getListByIds(LstSubscriptionTestIds);//test method getListByIds()
		DM003_Subscription.getListBySubscriptionNumber(LstSubsName);



	}
}