public class AP_Constant {
/*
----------------------------------------------------------------------
-- - Name          : AP_Constant
-- - Description   : Class for storing constants referenced in other  
--                   classes
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks  
-- -----------  ----  -------  ---------------------------------------
-- 19-JUN-2017  WCH    1.0      Initial version
----------------------------------------------------------------------

/***********************************************************************
* sending mail
************************************************************************/
    public static string webformTypeECOM = 'E-COM';
    public static string OrgWideDefault = 'Default';

    //method returns random unique string 
    public static String randomizeString(String name){        
        String charsForRandom = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < 6) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), charsForRandom.length());
           randStr += charsForRandom.substring(idx, idx+1);
        }
        return name+randStr;
    }

    //create admin user
    public static User createAdminUser(String name, Id profId){
        return new User(
            Username = AP_Constant.randomizeString(Name) +'@test.com',
            LastName = 'Ballack',
            FirstName = 'Jean',
            Email = 'Jean@test.com',
            Alias = 'JBAL',
            LanguageLocaleKey = 'en_US',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            ProfileId = profId
        );
    }

    //get for system administrator profile id
    public static id getProfileAdminId(){
        return ([Select Id From Profile 
                Where name = 'Administrateur système' 
                    OR name = 'System Administrator'
                    OR name = 'Amministratore del sistema'
                    OR name = 'Systemadministrator'
                    OR name = 'Systemadministratör'
                    OR name = 'Administrador do sistema'
                    OR name = 'Systeembeheerder'
                    OR name = 'Systemadministrator'].Id);
    }

    //method creates account
    public static Account createAccount(string name){
        return new Account(LastName = name, AccountCountry__c = 'UK');
    }

    //method returns map of recordtype.developername => recordtypeId
    public static Map<string,ID> MapRecordType(String ObjectName){
     
         List<RecordType> lstRecordType = new List<RecordType>();
         
         lstRecordType = [Select ID,SobjectType,Name,DeveloperName from RecordType where SobjectType=:ObjectName];
         
         Map<String,ID> MapRTCaseToDeveloperName = new Map<String,ID>();
         
         for(RecordType rec:lstRecordType)
         {
            MapRTCaseToDeveloperName.put(rec.DeveloperName,rec.ID);
         }
         
         return MapRTCaseToDeveloperName;
        
    } 
    
}