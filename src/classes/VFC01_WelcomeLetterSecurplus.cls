public class VFC01_WelcomeLetterSecurplus {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_WelcomeLetterSecurplus
-- - Description   : fetch all details on the object Zuora__Subscription__c
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 12-JUN-2017  WCH   1.0      Initial version
------------------------------------------------------------------------------------------------
*/
    public String strToday {get;set;}
    public String currentSubsId {get;set;}
    public Zuora__Subscription__c currentSub{get;set;}
    public Zuora__SubscriptionProductCharge__c currentSubProd{get;set;}

    //DMU: Added variable string to store concatenated value of field ProductName__c & RatePlanName__c
    //This requirement from SC_DXC_RCI_UAT Return_V4.xls asks to replace Zuora__ProductName_Product__c by concatenated value ProductName__c & RatePlanName__c
    public string prodRatePlanNameStr{get;set;}

    public VFC01_WelcomeLetterSecurplus() {
        currentSubsId = ApexPages.currentPage().getParameters().get('Id');
        try{
            currentSub = [SELECT   id
                            ,InsuredAccount__r.contactfunctionalid__pc
                            ,InsuredAccount__r.firstname
                            ,InsuredAccount__r.lastname
                            ,InsuredAccount__r.personemail
                            ,InsuredAccount__r.personmailingpostalcode
                            ,InsuredAccount__r.personmobilephone
                            ,Policy_Number__c
                            ,VehicleFirstRegistrationDate__c
                            ,VIN__c
                            ,YearOfManufacture__c
                            ,zuora__subscriptionstartdate__c
                            ,InvoiceNumber__c
                            ,Zuora__Account__r.Gender__pc
                            ,PlateNumber__c
                            ,Brand__c
                            ,Model__c
                            ,Zuora__Account__r.billingaddress
                            ,Zuora__Account__r.billingcity
                            ,Zuora__Account__r.billingpostalcode
                            ,Zuora__Account__r.billingstreet
                            ,Zuora__Account__r.contactfunctionalid__pc
                            ,Zuora__Account__r.personemail
                            ,Zuora__Account__r.personmailingpostalcode
                            ,Zuora__Account__r.personmobilephone
                            ,Zuora__Account__r.personbirthdate
                            ,Zuora__TermStartDate__c 
                            ,LastCoveredDay__c 
                            ,Zuora__OriginalCreated_Date__c
                            ,Zuora__SubscriptionNumber__c
                            ,Zuora__Account__r.lastname
                            ,Zuora__Account__r.firstname
                            ,Zuora__CustomerAccount__r.zuora__defaultpaymentmethod__c 
                            ,ProductName__c
                            ,PaymentMethod__c 
                            ,RatePlanName__c 
                            ,Zuora__Account__r.BillingAddressLine1__c
                            ,Zuora__Account__r.BillingAddressLine2__c
                            ,Zuora__Account__r.BillingAddressLine3__c
                            ,Zuora__Account__r.BillingAddressLine4__c
                            ,Zuora__Account__r.BillingPostalCode__c
                            ,Zuora__Account__r.BillingCity__c
                            ,Zuora__Account__r.BillingDistrict__c
                            ,Zuora__Account__r.BillingCountry__c
                            ,Zuora__Account__r.CompanyName__c 
                            ,Zuora__Account__r.Salutation
                   FROM Zuora__Subscription__c
                   WHERE id =:currentSubsId];

            prodRatePlanNameStr = currentSub.ProductName__c + ' ' + currentSub.RatePlanName__c;

            strToday = Datetime.now().format('dd/MM/YY');
            currentSubProd = [select Id
                                ,Zuora__Price__c
                                ,zuora__productname_product__c
                                ,Zuora__BillingPeriod__c
                                FROM Zuora__SubscriptionProductCharge__c
                                where Zuora__Subscription__c= : currentSubsId 
                                limit 1];
        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
        }
        
    }
}