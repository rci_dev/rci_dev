public with sharing class VFC01_EmailRemerciement {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_EmailRemerciement
-- - Description   : fetch all details on the object Zuora__Subscription__c
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 13-JUN-2017  ARA   1.0      Initial version
-- 20-JUN-2017  WCH   1.1      modify controller for visualforce component. Spec changed.
-- 14-SEP-2017  WCH   1.2      Zuora__Account__r.CompanyName__c Spec changed.
------------------------------------------------------------------------------------------------
*/

    public String currentSubsId;
    public Map<String,String> mapDocUrl;
    public Zuora__SubscriptionProductCharge__c currentSubProd;

    //DMU: added variable
    public Zuora__Subscription__c currentSubs {get;set;}
    //public Zuora__SubscriptionRatePlan__c currentSubRatePlan;    
    public String currentSubsPlanName {get;set;}
    public String currentSubProdName {get;set;}
    public String currentSubPNameToDisplay {get;set;}

    public void setcurrentSubsId (String s) {
        currentSubsId = s;
        setCurrentSubProd(currentSubsId);
    }

    public String getcurrentSubsId() {
        return currentSubsId;
    } 
    public Zuora__SubscriptionProductCharge__c getCurrentSubProd() {
        return currentSubProd;
    }
    public Map<String,String> getmapDocUrl() {
        return mapDocUrl;
    }
    public void setCurrentSubProd( Id zuoraSubscriptionId) {
        try{
            
            mapDocUrl = new map<String, String>();

            for (Document doc : [SELECT id, name FROM Document WHERE DeveloperName like 'EmailRemerciement_%']){
                mapDocUrl.put(doc.name, URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.ImageServer?id='+doc.Id+'&oid='+UserInfo.getOrganizationId());
            }
            
            currentSubProd = [select Id ,Zuora__Subscription__r.Zuora__SubscriptionNumber__c
                                        ,Zuora__Price__c 
                                        ,Zuora__ProductName_Product__c
                                        FROM Zuora__SubscriptionProductCharge__c
                                        where Zuora__Subscription__c= : currentSubsId 
                                        limit 1];
            //currentSubRatePlan = [select id, Zuora__SubscriptionRatePlanName__c from Zuora__SubscriptionRatePlan__c where Zuora__Subscription__c = : currentSubsId limit 1];
            
            //currentSubProdName = string.valueof(currentSubProd.Zuora__ProductName_Product__c).contains('SecurPlus') ? 'SecurPlus' :  (string.valueof(currentSubProd.Zuora__ProductName_Product__c).contains('CYBERRISK') ? 'CYBERRISK' :  (string.valueof(currentSubProd.Zuora__ProductName_Product__c).contains('Maintenance') ? 'Maintenance' : (string.valueof(currentSubProd.Zuora__ProductName_Product__c).contains('KeyCover') ? 'KeyCover' : null )));
            
            currentSubs = [select Id, Zuora__Account__r.LastName, Zuora__Account__r.FirstName, RatePlanName__c, ProductName__c, Policy_Number__c, VoucherNumber_PL__c, LastCoveredDay__c,Zuora__Account__r.CompanyName__c, SubscriberType__c, CompanyName__c from Zuora__Subscription__c where id =: currentSubsId];
            
            currentSubProdName = currentSubs.ProductName__c;
            currentSubsPlanName = currentSubs.RatePlanName__c;
            currentSubPNameToDisplay = currentSubs.ProductName__c + ' ' + currentSubs.RatePlanName__c;          
        }
        catch(Exception e){
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
           }
    }
}