/** 
* @author: Adolphe SONKO
* @date : Creation 01/08/2017
* @date : Modification 01/08/2017
* @description : This Class allows to declare the webservices methodes.
*/

public interface ITF001_getSubscriptionNumberFromInvoice {
    
  HttpResponse getPostSubscriptionNumber(HttpRequest request,  String responseType, Zuora.ZApi zApiInstance);
    
}