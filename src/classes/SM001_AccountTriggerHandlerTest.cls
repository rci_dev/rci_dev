/** 
* @author: Adolphe Sonko
* @date : Création 10/07/2017
* @date : Modification 10/07/2017
* @description : This Class allows to handle trigger events in Account entity.
*/
@isTest(SeeAllData=true)
public class SM001_AccountTriggerHandlerTest {
    
   //variables
  static Zuora__Subscription__c zuora1;
  static Zuora__Subscription__c zuora2;
  static list<Account> personList;
  static Zuora__CustomerAccount__c customerAccount1;
  static Zuora__CustomerAccount__c customerAccount2;
  static zqu__BillingEntity__c billingEntity;

   User testUser = TestFactory.getStandardUser();
//   insert testUser;
    
        @isTest
    static void SM001_AccountTriggerHandlertest(){
        //   system.runAs(testUser){
            /*SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                           'Complete','{"size":1,"totalSize":1,"done":true}',
                                                            null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);  */
            Test.startTest();
            
            // Data Preparation
            personList = new list<Account>{
                new Account(AccountCountry__c='DE',
                      AccountSource='Website',
                      BillingCity='Stuttgart',
                      BillingCountry='Germany',
                      BillingPostalCode='70173',
                      BillingStreet='175 Königstrasse',
                      Citizenship__pc='Germany',
                      EligibleToCrmActions__c=false,
                      FirstName='Yann',
                      Gender__pc='Male',
                      ContactFunctionalID__pc='JamesBond007',      
                      LastName='Martin',
                      PersonBirthdate=Date.today(),
                      PersonEmail='zdouiri@test.com',  
                      PersonMobilePhone='+44 20 7489 79 99',
                      Phone='07700 900709',
                      RecordTypeId=TestFactory.getSimpleRecordTypeIndividual().Id,
                      Salutation='Mr.',
                      Type='Prospect',
                      Tech_ZuoraAccountNumber__c='A00000016'),
                new Account(AccountCountry__c='UK',
                      AccountSource='Website',
                      Citizenship__pc='United Kingdom',
                      EligibleToCrmActions__c=false,  
                      FirstName='Giorgio',
                      Gender__pc='Male',
                      ContactFunctionalID__pc='JamesBond009', 
                      LastName='Torres',
                      PersonBirthdate=Date.today(),
                      PersonEmail='giorgio.torres@test.com',
                      PersonMobilePhone='+44 20 7489 79 99',
                      Phone='07700 900709',  
                      RecordTypeId=TestFactory.getSimpleRecordTypeIndividual().Id,
                      Salutation='Mr.',
                      Type='Client',
                      Tech_ZuoraAccountNumber__c='A00000015')
            };
            insert personList;
        
            customerAccount1 = new Zuora__CustomerAccount__c(Name='Yann Martin',
                                  Zuora__AccountNumber__c='A00000016',
                                  Zuora__Account__c=personList[0].Id,    
                                  Zuora__AutoPay__c=true,  
                                  Zuora__Balance__c=60.17,  
                                  Zuora__Batch__c='Batch1',  
                                  Zuora__BcdSettingOption__c='ManualSet',  
                                  Zuora__BillCycleDay__c='12th of the month',  
                                  Zuora__BillToAddress1__c='175 Königstrasse',  
                                  Zuora__BillToCity__c='Stuttgart',  
                                  Zuora__BillToCountry__c  ='Germany',  
                                  Zuora__BillToName__c='Yann Martin',  
                                  Zuora__BillToPostalCode__c='70173',    
                                  Zuora__BillToWorkEmail__c='  megbewole@csc.com',  
                                  Zuora__CreditBalance__c  =0.0,
                                  Zuora__Credit_Balance__c=0.0,
                                  Zuora__LastInvoiceDate__c=Date.today()+360,  
                                  Zuora__MRR__c=30.98,    
                                  Zuora__PaymentTerm__c='Due Upon Receipt',  
                                  Zuora__SoldToAddress1__c='  175 Königstrasse',  
                                  Zuora__SoldToCity__c='Stuttgart',  
                                  Zuora__SoldToCountry__c  ='Germany',
                                  Zuora__SoldToPostalCode__c='70173',  
                                  Zuora__SoldToWorkEmail__c='megbewole@csc.com',  
                                  Zuora__Status__c='Active',  
                                  Zuora__TaxExemptStatus__c='No',  
                                  Zuora__TotalInvoiceBalance__c=60.17);
        
            insert customerAccount1; 
        
            zuora1 = new Zuora__Subscription__c( InsuredAccount__c=personList[1].Id,
                              Name='A-S00000024',
                              Policy_Number__c='AFF/RCI/0001 - OPN_numer systemowy RCI',
                              Zuora__Account__c=personList[0].Id,
                              Zuora__ContractEffectiveDate__c=Date.today()+60,
                              Zuora__InitialTerm__c='12 Months',
                              Zuora__MRR__c=11.99,
                              Zuora__NextRenewalDate__c=  Date.today()+160,  
                              Zuora__OriginalCreated_Date__c=Date.today()+60,
                              Zuora__RenewalTermPeriodType__c='Month',
                              Zuora__RenewalTerm__c='6 Months',
                              Zuora__ServiceActivationDate__c=Date.today()+60,  
                              Zuora__Status__c='Active',
                              Zuora__SubscriptionEndDate__c=Date.today()+160,
                              Zuora__SubscriptionNumber__c='TEST_A-S10000024',
                              Zuora__SubscriptionStartDate__c=Date.today()+60,  
                              Zuora__TCV__c=143.11,
                              Zuora__TermEndDate__c=Date.today()+160,
                              Zuora__TermSettingType__c='TERMED',  
                              Zuora__TermStartDate__c=Date.today()+160,  
                              Zuora__Version__c=3.0,
                              Zuora__CustomerAccount__c=customerAccount1.Id,
                              Zuora__InvoiceOwner__c=customerAccount1.Id,
                              Zuora__Zuora_Id__c='2c92c0f95c65768c015c81f5dbb70bcf'); 
              

            customerAccount2 = new Zuora__CustomerAccount__c(Name='Veronique Coulon',
                                  Zuora__AccountNumber__c='A00000017',
                                  Zuora__Account__c=personList[1].Id,    
                                  Zuora__AutoPay__c=true,  
                                  Zuora__Balance__c=50.23,  
                                  Zuora__Batch__c='Batch1',  
                                  Zuora__BcdSettingOption__c='ManualSet',  
                                  Zuora__BillCycleDay__c='11th of the month',  
                                  Zuora__BillToAddress1__c='179 Königstrasse',  
                                  Zuora__BillToCity__c='Stuttgart',  
                                  Zuora__BillToCountry__c  ='Germany',  
                                  Zuora__BillToName__c='Veronique Coulon',  
                                  Zuora__BillToPostalCode__c='70179',    
                                  Zuora__BillToWorkEmail__c='  megbewole2@csc.com',  
                                  Zuora__CreditBalance__c  =0.0,
                                  Zuora__Credit_Balance__c=0.0,
                                  Zuora__LastInvoiceDate__c=Date.today()+360,  
                                  Zuora__MRR__c=30.98,    
                                  Zuora__PaymentTerm__c='Due Upon Receipt',  
                                  Zuora__SoldToAddress1__c='  179 Königstrasse',  
                                  Zuora__SoldToCity__c='Stuttgart',  
                                  Zuora__SoldToCountry__c  ='Germany',
                                  Zuora__SoldToPostalCode__c='70179',  
                                  Zuora__SoldToWorkEmail__c='megbewole2@csc.com',  
                                  Zuora__Status__c='Active',  
                                  Zuora__TaxExemptStatus__c='No',  
                                  Zuora__TotalInvoiceBalance__c=50.23);
        
            insert customerAccount2;
            
            zuora2= new Zuora__Subscription__c( InsuredAccount__c=personList[0].Id,
                              Name='A-S00000025',
                              Policy_Number__c='AFF/RCI/0002 - OPN_numer systemowy RCI',
                              Zuora__Account__c=personList[1].Id,
                              Zuora__ContractEffectiveDate__c=Date.today()+60,
                              Zuora__InitialTerm__c='12 Months',
                              Zuora__MRR__c=10.99,
                              Zuora__NextRenewalDate__c=  Date.today()+160,  
                              Zuora__OriginalCreated_Date__c=Date.today()+60,
                              Zuora__RenewalTermPeriodType__c='Month',
                              Zuora__RenewalTerm__c='6 Months',
                              Zuora__ServiceActivationDate__c=Date.today()+60,  
                              Zuora__Status__c='Active',
                              Zuora__SubscriptionEndDate__c=Date.today()+160,
                              Zuora__SubscriptionNumber__c='TEST_A-S10000025',
                              Zuora__SubscriptionStartDate__c=Date.today()+60,  
                              Zuora__TCV__c=143.11,
                              Zuora__TermEndDate__c=Date.today()+160,
                              Zuora__TermSettingType__c='TERMED',  
                              Zuora__TermStartDate__c=Date.today()+160,  
                              Zuora__Version__c=3.0,
                              Zuora__CustomerAccount__c=customerAccount2.Id,
                              Zuora__InvoiceOwner__c=customerAccount2.Id,
                              Zuora__Zuora_Id__c='2c92c0f95c65768c015c81f5dbb70bcg'); 
             
            
            //insert
            insert zuora1; 
            insert zuora2; 
            
            billingEntity = new zqu__BillingEntity__c(zqu__Depth__c = 1234,
                                                      Name = 'entity_default_name',
                                                      CurrencyIsoCode = 'GBP',
                                                      zqu__EntityID__c = 'testentity',
                                                      zqu__EntityName__c = 'entity_name',
                                                      zqu__GlobalEntityID__c = 'global_entity',
                                                      zqu__ParentEntityID__c = 'parent_entity',
                                                      zqu__ZuoraId__c = 'zuora_id');
            insert billingEntity;

            List<zqu__BillingEntity__c> insertedEntity = [SELECT ID, Name FROM zqu__BillingEntity__c WHERE Name='entity_default_name'];
            System.debug('## Inserted entity: ' + insertedEntity);
            
            
            //update
            personList[1].Tech_ZuoraAccountNumber__c = 'A00000017';
            update personList;


            Test.stopTest();
     //   }
    }
}