@isTest
private class EM002_InvoiceTest
{	
	static User testUser = TestFactory.getStandardUser();
	static List<Zuora__ZInvoice__c> testInvoices;
	static List<ID> testInvoicesId;

	@isTest
	static void EM002_InvoiceTest()
	{
		System.runAs(testUser)
		{
			//data preparation
			testInvoices = new List<Zuora__ZInvoice__c>{
				new Zuora__ZInvoice__c(Name = 'testInvoice'),
				new Zuora__ZInvoice__c(Name = 'testInvoice2')
			};
			insert testInvoices;

			testInvoicesId = new List<ID>();
			for(Zuora__ZInvoice__c inv : testInvoices){
				testInvoicesId.add(inv.Id);
			}

			EM002_Invoice.getListByInvoiceIds(testInvoicesId);
			//EM002_invoice.getRESTURL();
		}
	}
}