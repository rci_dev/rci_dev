public with sharing class VFC01_Voucher {
/*
------------------------------------------------------------------------------------------------
-- - Name          : VFC01_Voucher
-- - Description   : fetch all details on the object Zuora__Subscription__c
--
-- Maintenance History:
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -----------------------------------------------------------------
-- 13-JUN-2017  DMG   1.0      Initial version
------------------------------------------------------------------------------------------------
*/
    public String currentSubsId {get;set;}
    public Zuora__Subscription__c currentSub{get;set;}
 	
	public VFC01_Voucher() {
		currentSubsId = ApexPages.currentPage().getParameters().get('Id');
		try{
			currentSub = [SELECT   id
                        ,VoucherEndDate_PL__c
                        ,VoucherStartDate_PL__c
                        ,VoucherNumber_PL__c

               FROM Zuora__Subscription__c
               WHERE id = :currentSubsId];

		}
		catch(Exception e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'An error has occurred: '+e.getMessage()));
		   }

	}
}