/** 
* @author: Adolphe Sonko
* @date : Création 13/06/2017
* @date : Modification 13/06/2017
* @description : This trigger.
*/
trigger AccountTrigger on Account (after insert, after update) {
    
  if(!PAD.byPassAccountTriggerHandler){
        
        // after insert
        if(Trigger.isInsert && Trigger.isAfter){
            SM001_AccountTriggerHandler.onAfterInsert(Trigger.new);
        }
        
        // after update     
        if(Trigger.isUpdate && Trigger.isAfter){
            SM001_AccountTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);  
        }
        
  //end
    }
}