trigger RCI_SubscriptionProductCharge on Zuora__SubscriptionProductCharge__c (after insert, after update) {
	
    
    if(Trigger.isAfter){
        if(Trigger.isInsert || Trigger.isUpdate) {
            TRG_SubscriptionProductCharge.updateBSPFields (Trigger.new);
        }
    }
    
    
    
    if(Trigger.isBefore){
    }
}