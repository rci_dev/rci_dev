/** 
* @author: Adolphe Sonko
* @date : Création 02/02/2017
* @date : Modification 02/02/2017
* @description : This trigger .
*/
trigger Invoice2 on Zuora__ZInvoice__c (after insert) {
    
    if(!PAD.byPassInvoiceTriggerHandler){
        
        // after insert
        if(Trigger.isInsert && Trigger.isAfter){
            SM002_InvoiceTriggerHandler.onAfterInsert(Trigger.new);
        }
        
        // after update     
    /*    if(Trigger.isUpdate && Trigger.isAfter){
            SM001_AccountTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);  
        } */
        
  //end
    }

}