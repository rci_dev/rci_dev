trigger ZuoraSubscriptionTrigger on Zuora__Subscription__c (after insert,after update) {
/*
----------------------------------------------------------------------
-- - Name          : ZuoraSubscriptionAfterInsert  
-- - Author        : SC
-- - Description   : Actions to be done after insert of a Zuora__Subscription__c 
                 
-- Maintenance History:
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  ---------------------------------------
-- 19-JUN-2017  WCH    1.0      Intitial version
-- 21-JUN-2017  DMU    1.0      Add condtion for mail voucher 
----------------------------------------------------------------------*/
System.debug('## START OF TRIGGER Zuora__Subscription__c AFTER INSERT: ');
    //Trigger.isAfter && Trigger.isUpdate
    private Map<Id, Zuora__Subscription__c> updZuoraSubs; 
    private Map<Id, Id> zuoraSubNumMapZuoraAccount;
    Bypass__c userBypass = Bypass__c.getInstance(UserInfo.getUserId());
    System.debug('### userBypass: ' + userBypass.Bypass_Trigger__c);

    updZuoraSubs = new Map<Id, Zuora__Subscription__c>(); 
    zuoraSubNumMapZuoraAccount = new Map<Id, Id>();

    //NAK
    public list<Zuora__Subscription__c> zuoraSubLst = new list<Zuora__Subscription__c>();
    public set<Id> accIdSet = new set<Id>();

    for (Integer i = 0; i < trigger.new.size(); i++){
        if(userBypass.Bypass_Trigger__c == null || !userBypass.Bypass_Trigger__c.contains('AP01;')){
            
            if (Trigger.isUpdate) {
                //Added condition for PneuMini/PneuMaxi/CONCIERGE_SILVER/CONCIERGE_GOLD
                if(trigger.new[i].TECH_COUNT_ProductCharges__c>=1 && trigger.new[i].TECH_COUNT_RatePlan__c >=1 && trigger.new[i].Tech_SyncCommerceCloud__c && !String.isBlank(trigger.new[i].Zuora__Zuora_Id__c) && !String.isBlank(trigger.new[i].Policy_Number__c) && !trigger.new[i].Tech_EmailNotification__c && !AP01_SendSubscriptionEmail.boolRunOnce){
                    system.debug('enter trigger condition');
                    updZuoraSubs.put(trigger.new[i].Id,trigger.new[i]);
                    if(trigger.new[i].Zuora__Account__c!=null){
                        zuoraSubNumMapZuoraAccount.put(trigger.new[i].Id,trigger.new[i].Zuora__Account__c);
                    }
                }

                //Added condition for CYBERRISK/Securplus/MAINTENANCE/KeyProtection (similar conditions to line 29)
                if(trigger.new[i].TECH_COUNT_ProductCharges__c>=1 && trigger.new[i].TECH_COUNT_RatePlan__c >=1 && trigger.new[i].Tech_SyncCommerceCloud__c && !String.isBlank(trigger.new[i].Zuora__Zuora_Id__c) && !String.isBlank(trigger.new[i].Policy_Number__c) && !trigger.new[i].Tech_EmailNotification__c && !AP01_SendSubscriptionEmail.boolRunOnce){
                    updZuoraSubs.put(trigger.new[i].Id,trigger.new[i]);
                    if(trigger.new[i].Zuora__Account__c!=null){
                        zuoraSubNumMapZuoraAccount.put(trigger.new[i].Id,trigger.new[i].Zuora__Account__c);
                    }
                }

                //NAK evolution for Kwid
                if (trigger.new[i].Tech_SyncCommerceCloud__c 
                  && !String.isBlank(trigger.new[i].Zuora__Zuora_Id__c) 
                  && !String.isBlank(trigger.new[i].Policy_Number__c)
                  && !trigger.new[i].Tech_EmailNotificationVoucher__c){
                    zuoraSubLst.add(trigger.new[i]);
                    accIdSet.add(trigger.new[i].Zuora__Account__c);
                }
            } 
            }
            if (Trigger.isUpdate || Trigger.isInsert) {
                //Added condition for voucher
                if(trigger.new[i].Tech_SyncCommerceCloud__c 
                    && trigger.new[i].SubscriptionCountry__c == 'PL' 
                    && !String.isBlank(trigger.new[i].Zuora__Zuora_Id__c) 
                    && !String.isBlank(trigger.new[i].Policy_Number__c) 
                    && !String.isBlank(trigger.new[i].VoucherNumber_PL__c) 
                    && !trigger.new[i].Tech_EmailNotificationVoucher__c 
                    && !AP01_SendSubscriptionEmail.boolRunOnce){

                    updZuoraSubs.put(trigger.new[i].Id,trigger.new[i]);
                    if(trigger.new[i].Zuora__Account__c!=null){
                        zuoraSubNumMapZuoraAccount.put(trigger.new[i].Id,trigger.new[i].Zuora__Account__c);
                    }
                }
            }
 
    }//end for  
    
    if(userBypass.Bypass_Trigger__c == null || !userBypass.Bypass_Trigger__c.contains('AP01;')){
        if (updZuoraSubs.size() > 0){ 
            AP01_SendSubscriptionEmail.sendTyreInsuranceMail(updZuoraSubs,zuoraSubNumMapZuoraAccount);            
        }

        //NAK
        System.debug('## zuoraSubLst ' + zuoraSubLst);
        if (zuoraSubLst.size() > 0  && !AP01_SendRSIEmail.boolRunOnce){
            AP01_SendRSIEmail.sendEmail(zuoraSubLst,accIdSet);
        }
    }

}